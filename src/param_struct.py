import numpy as np
from numpy import pi

class params:
    def __init__(self, xyz, phi_det):
        self.nx=xyz[0]
        self.ny=xyz[1]
        self.nz=xyz[2]
        self.phi_det = phi_det
        self.num_of_segments = np.size(phi_det) #Number of angular segments 
        self.num_of_voxels = self.nx*self.ny*self.nz
    volume_upsampling = 1
    filter_2D = 3
    method = 'bilinear'
    avoid_wrapping=0
    opt_coeff=[1, 0, 0 ,0,]
    find_orientation=0
    find_grad=0
    regularization_angle=0
    find_orientation=0
    slice_bool=0
    slice_pos=0
    dispinter=1
    plot=1
    kernel=np.zeros((3,3))
    coeff_soft_limits_low=0
    coeff_soft_limits_high=1
    soft_limit_weight_coeff=1
    optimization_output_path="./"

class s_tensor:
    def __init__(self,l,m,data,theta_data,phi_data,mask3D):
        self.l=l
        self.m=m
        self.data=data
        self.theta_data=theta_data
        self.phi_data=phi_data
        self.mask3D=mask3D

class optim_in:
    optimize=1
    num_vox=1
    theta=np.zeros(1)
    phi=np.zeros(1)
    a=np.zeros(1)

# class sastt_projection:
#     class integ:

#     class par:

#     class rot_x:

#     class rot_y:


    
