#include <stdio.h>
#include "bilinear_allocation.h"
#include <math.h>
#include <stdlib.h>

void arb_projection(double * restrict data_synt, double * restrict X, double * restrict Y,
 					double * restrict Z, double * R, double * xout, double * yout, double * projection_out, 
					int num_voxels, int num_segments, int * n_dims, int * n_xout_yout){
	double * Xp, * Yp, * Xp_up, * Yp_up, * Tx, * Ty, * projection_upsampled;
	double * Ax, * Ay;

 	Xp = (double *)calloc((num_voxels),sizeof(double));
 	Yp = (double *)calloc((num_voxels),sizeof(double));
 	projection_upsampled = (double *)calloc(((n_xout_yout[0]*2+3)*(n_xout_yout[1]*2+3))*num_segments,sizeof(double));


 	int i;

 	for(i=0;i<num_voxels;i++){
 		Xp[i] = 2*(X[i]*R[0] + Y[i]*R[1] + Z[i]*R[2]);
 		Yp[i] = 2*(X[i]*R[3] + Y[i]*R[4] + Z[i]*R[5]);
 	}


 	int uval = 2,dims_size=4;
 	int NDims[4];
 	for (int aw = 0; aw < 3; ++aw)
 	{
 		NDims[aw] = n_dims[aw];
 	}
 	NDims[3] = num_segments;
 	//printf("ndims: %d, %d, \n",n_xout_yout[0],n_xout_yout[1]);
 	double * data_synt_upsampled; 	
 	data_synt_upsampled = (double *)calloc(((n_dims[0]*2+1)*(n_dims[1]*2+1)*(n_dims[2]*2+1)*(num_segments)),sizeof(double));
 	upsample(data_synt_upsampled,data_synt,NDims,&dims_size,&uval);

 	dims_size=3; 	

 	Xp_up = (double *)calloc((n_dims[0]*2+1)*(n_dims[1]*2+1)*(n_dims[2]*2+1),sizeof(double));
 	Yp_up = (double *)calloc((n_dims[0]*2+1)*(n_dims[1]*2+1)*(n_dims[2]*2+1),sizeof(double)); 	
 	//printf("Which upsample?\n");
 	upsample(Xp_up,Xp,NDims,&dims_size,&uval);

 	upsample(Yp_up,Yp,NDims,&dims_size,&uval);

 	double * xout_new,* yout_new;
 	xout_new = (double*)calloc((n_xout_yout[0]*2+2),sizeof(double));
 	yout_new = (double*)calloc((n_xout_yout[1]*2+2),sizeof(double));

 	for(i=0;i<n_xout_yout[0]*2;i++){
 		xout_new[i] = (2*xout[0]+i);
 		// printf("xon: %f\n", xout_new[i]);
 	}
 	for(i=0;i<n_xout_yout[1]*2;i++){
 		yout_new[i] = (2*yout[0]+i);
 		// printf("yon: %f\n", yout_new[i]);
 	}

 		// printf("xout_new, yout_new: %e, %e\n", xout_new[0],yout_new[0]);

 	Ax = (double *)calloc((num_voxels)*8,sizeof(double));
 	Ay = (double *)calloc((num_voxels)*8,sizeof(double));
 	Tx = (double *)calloc((num_voxels)*8,sizeof(double));
 	Ty = (double *)calloc((num_voxels)*8,sizeof(double));
 	// for (int xx = 0; xx < 6; ++xx)
 	// {
 	// 	f = (Xp_up[xx]-(double) xout_new[0]);
 	// 	printf("%f\n",f);
 	// }
 	for(i=0;i<num_voxels*8;i++){
 		Ax[i] = floorf(Xp_up[i]-xout_new[0]);
 		Ay[i] = floorf(Yp_up[i]-yout_new[0]);
 		Tx[i] =  (Xp_up[i]-xout_new[0])-Ax[i];
 		Ty[i] =  (Yp_up[i]-yout_new[0])-Ay[i];
 	} 	 	
 	// for (i = 3000; i < 3006; ++i)
 	// {
 	// 	printf("Ax, Ay: %e, %e\n", Ax[i],Ay[i]);
 	// }
 	free(xout_new);
 	free(yout_new);
 	free(Xp);
 	free(Xp_up);
 	free(Yp); 	
 	free(Yp_up);
 	int all_size=(num_voxels)*8;
 	int * proj_size;
 	proj_size = (int *) malloc(sizeof(int)*3);
 	proj_size[0] = n_xout_yout[0]*2;
 	proj_size[1] = n_xout_yout[1]*2;
 	proj_size[2] = num_segments;

 	//printf("size: %d, %d ,%d\n", proj_size[0], proj_size[1], proj_size[2]);
 	//printf("before alloc: %f\n", projection_upsampled[95]);
 	allocate(projection_upsampled,proj_size, data_synt_upsampled,&all_size,Ax,Ay,Tx,Ty);

 	free(Ax);
 	free(Ay);
 	free(Tx);
 	free(Ty);

 	free(data_synt_upsampled);
 	int proj_size_upsampled[3] = {proj_size[0],proj_size[1],num_segments};
 	double kernel[3] = {1./6., 2./3., 1./6.};
 	double kernelfull[9];

 	for(i=0;i<3;i++){
 		for (int j = 0; j < 3; j++)
 		{
 			kernelfull[i*3 + j] = kernel[i]*kernel[j]/1.;
 		}
 	}
 	int kernel_size = 3;
 	//printf("before downsample: %f\n", projection_upsampled[95]);
 	convolve_downsample_2d(projection_out,projection_upsampled,kernelfull,proj_size_upsampled,kernel_size);

 	free(projection_upsampled);
 	free(proj_size);
 	return;

}
void arb_back_projection(double * restrict tomo_obj, double * restrict proj_obj, int * restrict proj_size, double * restrict X, double * restrict Y,
 					double * restrict Z, double * R, double * xout, double * yout,
					int num_voxels, int * n_dims, int num_segments){
	double * Xp, * Yp, * Tx, * Ty;
	double * Ax, * Ay;
 	Xp = (double *)malloc(sizeof(double)*(num_voxels));
 	Yp = (double *)malloc(sizeof(double)*(num_voxels));
 	Ax = (double *)malloc(sizeof(double)*(num_voxels));
 	Ay = (double *)malloc(sizeof(double)*(num_voxels));
 	Tx = (double *)malloc(sizeof(double)*(num_voxels));
 	Ty = (double *)malloc(sizeof(double)*(num_voxels));

 	int i;

 	for(i=0;i<num_voxels;i++){
 		Xp[i] = (X[i]*R[0] + Y[i]*R[1] + Z[i]*R[2]);
 		Yp[i] = (X[i]*R[3] + Y[i]*R[4] + Z[i]*R[5]);
 	}
 	for(i=0;i<num_voxels;i++){
 		Ax[i] = floorf(Xp[i]-xout[0]);
 		Ay[i] = floorf(Yp[i]-yout[0]);
 		Tx[i] = (double) (Xp[i]-xout[0])-Ax[i];
 		Ty[i] = (double) (Yp[i]-yout[0])-Ay[i];
 	}

 	int dims[4] = {n_dims[0],  n_dims[1], n_dims[2], num_segments};
 	allocate_back(tomo_obj, dims,proj_size,proj_obj, &num_voxels,Ax,Ay,Tx,Ty);
 	free(Xp);
 	free(Yp);
 	free(Ax);
 	free(Ay);
 	free(Tx);
 	free(Ty);

 	return;

}