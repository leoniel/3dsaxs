# If this code, or subfunctions or parts of it, is used for research in a 
#   publication or if it is fully or partially rewritten for another 
#   computing language the authors and institution should be acknowledged 
#   in written form and additionally you should cite:
#     M. Liebi, M. Georgiadis, A. Menzel, P. Schneider, J. Kohlbrecher, 
#     O. Bunk, and M. Guizar-Sicairos, “Nanostructure surveys of 
#     macroscopic specimens by small-angle scattering tensor tomography,”
#     Nature 527, 349-352 (2015).   (doi:10.1038/nature16056)
#
#
#
#

import numpy as np
from numpy import pi
import param_struct as ps
from scipy.interpolate import RegularGridInterpolator
from scipy.signal import convolve2d,decimate
from scipy.spatial.transform import Rotation as Ro
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from ctypes import *
import time
import ctypes

so_file="./bilinear_allocation.so"
myfun = CDLL(so_file)

def sum_projection(size_proj_out_all, tomo_obj, Ax, Ay, Tx, Ty): 
    nRows = int(np.floor(size_proj_out_all[0]))
    nCols = int(size_proj_out_all[1])
    nPages = int(size_proj_out_all[2])
    page_out = nRows*nCols
    page_in = Ax.shape[1]*Ax.shape[2]
    proj_out=np.zeros(size_proj_out_all)
    shift_in=0
    shift_out=0
    print(proj_out.shape)
    print(tomo_obj.shape)
    print(Ax.shape)
    for i,j,k,l in np.ndindex(tomo_obj.shape):
        if ((Ax[i,j,k] > 0)  and  (Ax[i,j,k] < nCols)  and  (Ay[i,j,k] > 0)  and  (Ay[i,j,k] < nRows)):
            temp1 = Tx[i,j,k]*Ty[i,j,k]
            temp2 = Tx[i,j,k]*(1-Ty[i,j,k])
            temp3 = (1-Tx[i,j,k])*Ty[i,j,k]
            temp4 = (1-Tx[i,j,k])*(1-Ty[i,j,k])
            proj_out[int(Ax[i,j,k]),int(Ay[i,j,k]),l] += tomo_obj[i,j,k,l] * temp1
            proj_out[int(Ax[i,j,k])-1,int(Ay[i,j,k]),l] += tomo_obj[i,j,k,l] * temp2
            proj_out[int(Ax[i,j,k]),int(Ay[i,j,k])-1,l] += tomo_obj[i,j,k,l] * temp3
            proj_out[int(Ax[i,j,k])-1,int(Ay[i,j,k])-1,l] += tomo_obj[i,j,k,l] * temp4
    return proj_out

def arb_projection(tomo_obj_all, X, Y, Z, R, p, yout, xout):
    """
    tomo_obj_all - x, y, z, (a)
    X, Y, Z - array dimensions, integers
    R - 3x3 rotation matrix
    p - projection parameters
    p.volume_upsampling = 1 means upsample, 0 means don't
    p.filter_2D = 0 to 3 where 0 means no filter and 3 means max filter
    p.method = 'nearest' or 'bilinear'
    Recommended settings:
    Fast & low artifacts & low resolution
    p.volume_upsampling = 0
    p.filter_2D = 3           # Lower values give you compromise between resolution and artifacts
    p.method = 'bilinear'     # 'nearest' or 'bilinear'
 
    Slow & low artifacts & high resolution ###
    Still gives some artifacts at 45 degrees if only rotated around z
    p.volume_upsampling = 1
    p.filter_2D = 3            #Lower values give you compromise between resolution and artifacts
    p.method = 'bilinear'      #'nearest' or 'bilinear'
    """
    if not(isinstance(p,ps.params)):
        raise TypeError('p must be a "params" type class!') #this structure type will always have the default values
        return
    if not(isinstance(tomo_obj_all,np.ndarray)):
        raise TypeError('tomo_obj_all must be an "np.ndarray" type class!') #needed for advanced indexing
        return
    if not(isinstance(X,np.ndarray) and isinstance(Y,np.ndarray) and isinstance(Z,np.ndarray)):
        raise TypeError('X, Y, Z must all be "np.ndarray" type classes!') #needed for advanced indexing
        return    
    if not(np.issubdtype(X[0,0].dtype,int) and np.issubdtype(Y[0,0].dtype,int) and np.issubdtype(Z[0,0].dtype,int)):
        raise TypeError('X, Y, Z must all contain integers of integer type!') #needed for advanced indexing
        return
    if not(np.all((tomo_obj_all[:,:,:,1].size)==X.size) and np.all((tomo_obj_all[:,:,:,1].size)==Y.size) and np.all((tomo_obj_all[:,:,:,1].size)==Z.size)):
        raise IndexError('tomo_obj_all[:,:,:,1] must have the same size as X, Y and Z!')
        return
    if not((abs(X[1,1,1]-X[0,0,0])==1) and (abs(Y[1,1,1]-Y[0,0,0])==1) and (abs(Z[1,1,1]-Z[0,0,0])==1)):
        raise ValueError('Spacing between consecutive coordinates is not 1')
    if (np.array(xout).size==1 or np.array(yout).size==1):
        if abs(X[1,0,0]-X[0,0,0])==1:
            xout = X[:,0,0]
        else:
            if abs(X[0,1,0]-X[0,0,0])==1:
                xout = X[0,:,0]
            else:
                if abs(X[0,0,1]-X[0,0,0])==1:
                    xout = X[0,0,:]
                else:
                    raise IndexError('Could not determine along which array dimension X runs')
        if abs(Y[1,0,0]-Y[0,0,0])==1:
            yout = Y[:,0,0]
        else:
            if abs(Y[0,1,0]-Y[0,0,0])==1:
                yout = Y[0,:,0]
            else:
                if abs(Y[0,0,1]-Y[0,0,0])==1:
                    yout = Y[0,0,:]
                else:
                    raise IndexError('Could not determine along which array dimension Y runs')
    else:
        if not(abs(xout[0]-xout[1]) == 1) or not(abs(yout[0] - yout[1]) == 1):
            print((abs(xout[0]-xout[1])))
            raise ValueError("xout and yout must be spaced in steps of 1! Use np.arange(min,max,1)!")
    #print(Y[0,:,0])
    #print(X[:,0,0])
    # Generate coordinates for rotated object
    Xp = R[0,0]*X + R[0,1]*Y + R[0,2]*Z
    Yp = R[1,0]*X + R[1,1]*Y + R[1,2]*Z
    #print((Y[0:5,0,0]))
    #print((Y[0,0,0:5]))
    #print((Y[0,0:5,0]))
    #print(np.min(Y))
    #print(np.min(xout))
    #print(np.min(yout))
    t = time.time()
    xout_orig = xout
    yout_orig = yout
    for nnn in range(1):
        if p.volume_upsampling:
            xout_orig = xout
            xout = np.arange(2*xout_orig[0],2*xout_orig[-1]+2,1)
            yout_orig = yout
            yout = np.arange(2*yout_orig[0],2*yout_orig[-1]+2,1)
            pdims=np.array(np.shape(tomo_obj_all))
            pdims2=np.array(np.shape(Xp))
            pdims3=np.array(np.shape(Yp))
            p_out=np.zeros((pdims[0]*2,pdims[1]*2,pdims[2]*2,pdims[3]))
            p_out_2=np.zeros(np.add(1,np.multiply(np.shape(Xp),2)))
            p_out_3=np.zeros(np.add(1,np.multiply((np.shape(Yp)),2)))
            in_obj=tomo_obj_all
            in_obj=in_obj.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
            dims_in=pdims.ctypes.data_as(ctypes.POINTER(ctypes.c_int32))
            dims_in_2=pdims2.ctypes.data_as(ctypes.POINTER(ctypes.c_int32))
            dims_in_3=pdims3.ctypes.data_as(ctypes.POINTER(ctypes.c_int32))
            dims_size=np.array(pdims.size).ctypes.data_as(ctypes.POINTER(ctypes.c_int32))
            dims_size_2=np.array(pdims2.size).ctypes.data_as(ctypes.POINTER(ctypes.c_int32))
            dims_size_3=np.array(pdims3.size).ctypes.data_as(ctypes.POINTER(ctypes.c_int32))
            uval=np.array(2).ctypes.data_as(ctypes.POINTER(ctypes.c_int32))
            p_out=p_out.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
            p_out_2=p_out_2.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
            p_out_3=p_out_3.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
            
            myfun.upsample(p_out,in_obj,dims_in,dims_size,uval)
            tomo_obj_all=np.ctypeslib.as_array(p_out,(pdims[0]*2,pdims[1]*2,pdims[2]*2,pdims[3]))
            
            # fig = plt.figure()
            # x=fig.add_subplot(111,projection='3d')
            # x.voxels(tomo_obj_all[:,:,:,0])
            # plt.show()
            in_obj=Xp*2
            in_obj=in_obj.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
            myfun.upsample(p_out_2,in_obj,dims_in_2,dims_size_2,uval)
            Xp=np.ctypeslib.as_array(p_out_2,(pdims2[0]*2+1,pdims2[1]*2+1,pdims2[2]*2+1))
            in_obj=Yp*2
            in_obj=in_obj.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
            myfun.upsample(p_out_3,in_obj,dims_in_3,dims_size_3,uval)
            Yp=np.ctypeslib.as_array(p_out_3,(pdims2[0]*2+1,pdims2[1]*2+1,pdims2[2]*2+1))
    print("Time elapsed: ",-t+time.time())
            
    min_xout = np.min(xout);
    min_yout = np.min(yout);
    print(min_xout)
    print(min_yout)
    print(np.max(Xp))
    print(np.max(Yp))
    if p.method is "nearest":
        Ax = np.round(Xp-min_xout+1).astype(int) #Index of output image that corresponds to the voxel
        Ay = np.round(Yp-min_yout+1).astype(int) #Index of output image that corresponds to the voxel
    
        #initialization for all iterations of the following loop
        proj_out_all = np.zeros((np.size(xout), np.size(yout), np.size(tomo_obj_all, 3)));
        for jj in range(np.size(tomo_obj_all,3)):
            for i in range(np.size(Ax,0)):
                for j in range(np.size(Ax,1)):
                    for k in range(np.size(Ax,2)):
                        if ((Ax[i,j,k] > 0)  and  (Ax[i,j,k] < np.size(proj_out_all,1)))  and  (Ay[i,j,k] < np.size(proj_out_all,0)):
                            proj_out_all[Ax[i,j,k],Ay[i,j,k],jj] = proj_out_all[Ax[i,j,k],Ay[i,j,k],jj] + tomo_obj_all[i,j,k,jj]
    else:
        if p.method is "bilinear":
            ### Bilinear interpolation
            Ax = np.subtract(Xp,min_xout) # Index of output image that corresponds to the voxel
            tallsize=np.array(np.size(Ax)).ctypes.data_as(ctypes.POINTER(ctypes.c_int32))
            Ay = np.subtract(Yp,min_yout) # Index of output image that corresponds to the voxel
            #print(np.size(xout)*np.size(yout)*np.size(tomo_obj_all,3)*50)
            Tx = np.subtract(np.subtract(Xp,min_xout),Ax)
            #print(Ax)
            Tx = Tx.ctypes.data_as(ctypes.POINTER(ctypes.c_double)) # Variable from 0 to 1 from x distance of pixel Ax to Ax+1 where the voxel hits

            Ty = ((Yp-min_yout) - Ay).ctypes.data_as(ctypes.POINTER(ctypes.c_double))            
            Ax = Ax.ctypes.data_as(ctypes.POINTER(ctypes.c_double)) # Index of output image that corresponds to the voxel
            Ay = Ay.ctypes.data_as(ctypes.POINTER(ctypes.c_double)) # Index of output image that corresponds to the voxel
            size_proj_out_all_p = (np.array((np.size(xout), np.size(yout), np.size(tomo_obj_all, 3))))
            #proj_out_all = sum_projection(size_proj_out_all, tomo_obj_all, Ax, Ay, Tx, Ty)
            proj_out_all=np.zeros(size_proj_out_all_p).ctypes.data_as(ctypes.POINTER(ctypes.c_double))
            size_proj_out_all=size_proj_out_all_p.ctypes.data_as(ctypes.POINTER(ctypes.c_int32))
            page_in=np.size(tomo_obj_all,2)
            s_tom=tomo_obj_all.shape
            tomo_obj_all=tomo_obj_all.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
            t = time.time()
            myfun.allocate(proj_out_all, size_proj_out_all, tomo_obj_all,tallsize, Ax,Ay,Tx,Ty)
            print("Time elapsed: ",-t+time.time())

            proj_out_all=np.ctypeslib.as_array(proj_out_all,size_proj_out_all_p)
            tomo_obj_all=np.ctypeslib.as_array(tomo_obj_all,s_tom)
            # fig = plt.figure()
            # plt.imshow((np.nan_to_num(proj_out_all[:,:,1])),"jet")
            # plt.show()

        else:
            raise ValueError("Method not recognized!")
    if p.filter_2D == 0:
        filt_2D = np.array((1,1,1))
    else:
        if p.filter_2D == 1:
            filt_2D = np.array((0.25,1,0.25))
            print("out")
        else:
            if p.filter_2D == 2:
                filt_2D = np.array((0.5,1,0.5))
            else:
                if p.filter_2D == 3:
                    filt_2D = np.array((np.divide(1,3), np.divide(2,3), 1, np.divide(2,3), np.divide(1,3)))/3
                    filt_2D = filt_2D.reshape(filt_2D.size,1)@filt_2D.reshape(1,filt_2D.size)
                    print(filt_2D)
    if p.filter_2D > 0:

        # print("Filtering...")
        # filt_2D = filt_2D.reshape(filt_2D.size,1)@filt_2D.reshape(1,filt_2D.size)
        # filt_2D = np.divide(filt_2D,np.sum(filt_2D))
        # fig = plt.figure()
        # plt.imshow((proj_out_all[:,:,0]),"jet")
        # fig = plt.figure()
        # plt.imshow((proj_out_all[:,:,1]),"jet") 
        temp=np.zeros(((proj_out_all.shape[0])//2,(proj_out_all.shape[1])//2,proj_out_all.shape[2]))
        filt_2D = filt_2D.ctypes.data_as(ctypes.POINTER(ctypes.c_double))           
        t = time.time()
        proj_out_temp=np.zeros(((proj_out_all.shape[0])//2,(proj_out_all.shape[1])//2))
        proj_out_temp=proj_out_temp.ctypes.data_as(ctypes.POINTER(ctypes.c_double))    
        proj_out_in=np.copy(proj_out_all[:,:,:])
        print(np.sum(proj_out_in))
        print(proj_out_in.shape)
        im_dims = (np.array(proj_out_in.shape)+np.array([0,0,0])).ctypes.data_as(ctypes.POINTER(ctypes.c_long))
        print(proj_out_in.shape)
        proj_out_in=proj_out_in.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        # kern_size=5
        # kern_size=np.array(kern_size).ctypes.data_as(ctypes.POINTER(ctypes.c_long))

        myfun.convolve_downsample_2d(proj_out_temp,proj_out_in,filt_2D,im_dims,5)
        proj_out_temp=np.ctypeslib.as_array(proj_out_temp,((proj_out_all.shape[0])//2,(proj_out_all.shape[1])//2,(proj_out_all.shape[2])))
        fig = plt.figure()
        plt.imshow((proj_out_temp[:,:,1]),"jet")
        fig = plt.figure()
        plt.imshow((proj_out_all[:,:,1]),"jet")
        plt.show()
        print("Time elapsed: ",-t+time.time())
        # temp[:,:,jj] = np.copy(proj_out_temp).transpose()
        # print(np.sum(temp[:,:,:]))
       #     proj_out_all[:,:,jj] = convolve2d(proj_out_all[:,:,jj], filt_2D, 'same')

    # if p.volume_upsampling:
    #     for jj in range(np.size(proj_out_all,2)):
    #         #print(np.shape(proj_out_all))
    #         temp[:,:,jj] = decimate(decimate(proj_out_all[:,:,jj],2,axis=0),2,axis=1)
            #print(np.shape(proj_out_all))
        #proj_out_all=proj_out_all[0::2,0::2,:]
    proj_out_all=temp
    xout = xout_orig
    yout = yout_orig
    return proj_out_all

def arb_back_projection(proj_obj, x, y, X, Y, Z, R, p):
    """
    tomo_obj_all - x, y, z, (a)
    X, Y, Z - array dimensions, integers
    R - 3x3 rotation matrix
    p - projection parameters
    p.volume_upsampling = 1 means upsample, 0 means don't
    p.filter_2D = 0 to 3 where 0 means no filter and 3 means max filter
    p.method = 'nearest' or 'bilinear'
    Recommended settings:
    Fast & low artifacts & low resolution
    p.volume_upsampling = 0
    p.filter_2D = 3           # Lower values give you compromise between resolution and artifacts
    p.method = 'bilinear'     # 'nearest' or 'bilinear'
 
    Slow & low artifacts & high resolution ###
    Still gives some artifacts at 45 degrees if only rotated around z
    p.volume_upsampling = 1
    p.filter_2D = 3            #Lower values give you compromise between resolution and artifacts
    p.method = 'bilinear'      #'nearest' or 'bilinear'
    """
    if not(isinstance(p,ps.params)):
        raise TypeError('p must be a "params" type class!') #this structure type will always have the default values
        return
    if not(isinstance(tomo_obj_all,np.ndarray)):
        raise TypeError('tomo_obj_all must be an "np.ndarray" type class!') #needed for advanced indexing
        return
    if not(isinstance(X,np.ndarray) and isinstance(Y,np.ndarray) and isinstance(Z,np.ndarray)):
        raise TypeError('X, Y, Z must all be "np.ndarray" type classes!') #needed for advanced indexing
        return    
    if not(np.issubdtype(X[0,0].dtype,int) and np.issubdtype(Y[0,0].dtype,int) and np.issubdtype(Z[0,0].dtype,int)):
        raise TypeError('X, Y, Z must all contain integers of integer type!') #needed for advanced indexing
        return
    if not((abs(X[1,1,1]-X[0,0,0])==1) and (abs(Y[1,1,1]-Y[0,0,0])==1) and (abs(Z[1,1,1]-Z[0,0,0])==1)):
        raise ValueError('Spacing between consecutive coordinates is not 1')
    #print(Y[0,:,0])
    #print(X[:,0,0])
    # Generate coordinates for rotated object
    Xp = R[0,0]*X + R[0,1]*Y + R[0,2]*Z
    Yp = R[1,0]*X + R[1,1]*Y + R[1,2]*Z
    #print((Y[0:5,0,0]))
    #print((Y[0,0,0:5]))
    #print((Y[0,0:5,0]))
    #print(np.min(Y))
    #print(np.min(xout))
    #print(np.min(yout))
    t = time.time()
    min_x = np.min(x);
    min_y = np.min(y);
    if p.method is "bilinear":
        ### Bilinear interpolation
        Ax = np.subtract(Xp,min_x).astype(int) # Index of output image that corresponds to the voxel
        tallsize=np.array(np.size(Ax)).ctypes.data_as(ctypes.POINTER(ctypes.c_int32))
        Ay = np.subtract(Yp,min_y).astype(int) # Index of output image that corresponds to the voxel
        #print(np.size(xout)*np.size(yout)*np.size(tomo_obj_all,3)*50)
        Tx = np.subtract(np.subtract(Xp,min_x),Ax)
        #print(Ax)
        s_proj=np.shape(proj_obj)
        Tx = Tx.ctypes.data_as(ctypes.POINTER(ctypes.c_double)) # Variable from 0 to 1 from x distance of pixel Ax to Ax+1 where the voxel hits
        Ty = ((Yp-min_y) - Ay).ctypes.data_as(ctypes.POINTER(ctypes.c_double))            
        Ax = Ax.ctypes.data_as(ctypes.POINTER(ctypes.c_int32)) # Index of output image that corresponds to the voxel
        Ay = Ay.ctypes.data_as(ctypes.POINTER(ctypes.c_int32)) # Index of output image that corresponds to the voxel
        size_tomo_out_all_p = (np.array((np.size(X,0),np.size(X,1),np.size(X,2), np.size(proj_obj, 2))))
        #proj_out_all = sum_projection(size_proj_out_all, tomo_obj_all, Ax, Ay, Tx, Ty)
        tomo_out=np.zeros(size_tomo_out_all_p).ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        size_tomo_out_all=size_tomo_out_all_p.ctypes.data_as(ctypes.POINTER(ctypes.c_int32))
        proj_obj=proj_obj.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        t = time.time()
        print(s_proj)
        print(size_tomo_out_all_p)
        print("")
        myfun.allocateBack(tomo_out, size_tomo_out_all, proj_obj,tallsize,Ax,Ay,Tx,Ty)

        print("Time elapsed: ",-t+time.time())        

        tomo_out=np.ctypeslib.as_array(tomo_out,size_tomo_out_all_p)
        proj_obj=np.ctypeslib.as_array(proj_obj,s_proj)
        

    else:
        raise ValueError("Method not recognized!")

    return tomo_out,proj_obj

p = ps.params((100,100,100),(0,pi))
print(isinstance(p,ps.params))
p.volume_upsampling=1
#p.method="nearest"
p.filter_2D =3

# # Define coordinates, center of rotation at zero
N = np.array((75, 75, 75, 2)); # Size of tomogram
x = (np.arange(N[0])-np.ceil(N[0]/2)).astype(int)
y = (np.arange(N[1])-np.ceil(N[1]/2)).astype(int)
z = (np.arange(N[2])-np.ceil(N[2]/2)).astype(int)
print(x.dtype)
X, Y, Z = np.meshgrid(x,y,z,indexing='ij')
#
tomo_obj_all=np.zeros((N))
print(tomo_obj_all.shape)
print(X.shape)
# # Define object
# # tomo_obj = double((X**2+Y**2+Z**2)<=20^2);
# # tomo_obj = tomo_obj+double((X**2+(Y-20)**2+Z**2)<=10^2);
tomo_obj_all[:,:,:,0] =  np.exp(-((X**2+Y**2+Z**2)/(15**2))**15)
tomo_obj_all[:,:,:,0] += np.exp(-((X**2+(Y-15)**2+Z**2)/(8**2))**15)
tomo_obj_all[:,:,:,1] =  np.exp(-((X**2+Y**2+Z**2)/(8**2))**15)
tomo_obj_all[:,:,:,1] += np.exp(-(((X-8)**2+(Y+8)**2+Z**2)/(8**2))**15)
xout=0
yout=0  
R=Ro.from_rotvec((0,0,0)).as_matrix()
R1=Ro.from_rotvec((0,0,0)).as_matrix()
R2=Ro.from_rotvec((pi/30,-pi/30,0)).as_matrix()
# R1=Ro.from_rotvec((pi/4,-pi/30,0)).as_matrix()
# R2=Ro.from_rotvec((pi/4,pi/30,0)).as_matrix()

xout=np.arange(-50,50,1,dtype=int)
yout=np.arange(-50,50,1,dtype=int)
#t = time.time()
# fig = plt.figure()
# x=fig.add_subplot(111,projection='3d')
# x.voxels(tomo_obj_all[:,:,:,0])
# plt.show()

# x=fig.add_subplot(111,projection='3d')

# x.voxels(tomo_obj_all[:,:,:,0])
AAA = arb_projection(tomo_obj_all, X, Y, Z, R1, p, yout, xout)
# do stuff
#elapsed = time.time() - t
#print("Time elapsed: ",elapsed)
# #print(AAA.shape)
# fig = plt.figure()
# AAA=AAA
# np.nan_to_num(AAA,copy=False)
# AAY=AAA
# print(AAA.shape)

# fig = plt.figure()
# plt.imshow((AAA[:,:,1]),"jet")
# print("testagainnow2")
#fig = plt.figure()
#plt.imshow((AAA[:,:,1]).transpose(),"jet")
##fig = plt.figure()
##x=fig.add_subplot(111,projection='3d')
##x.voxels(tomo_obj_all[:,:,:,0])
##fig = plt.figure()
##ax2=fig.add_subplot(111,projection='3d')
##ax2.voxels(tomo_obj_all[:,:,:,1])
# AAAi=np.arange(0,np.size(AAA),1).reshape(np.shape(AAA))
# AAAi[:,:,0]=AAAi[:,:,0].transpose().astype(int)
toto,proj_obj=arb_back_projection(AAA.astype(float), xout, yout, X, Y, Z, R1, p)
# print(toto[1:10,1:10,1:10,0])
fig=plt.figure()
x=fig.add_subplot(111,projection='3d')

x.voxels(toto[:,:,:,0]>1.)
# fig=plt.figure()
# plt.imshow(toto[:,:,50,0].transpose(),"jet")
# fig=plt.figure()
# plt.imshow(proj_obj[:,:,0].transpose(),"jet")
plt.show()
