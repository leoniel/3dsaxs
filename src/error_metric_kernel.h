#ifndef _error_metric_kernel
#define _error_metric_kernel


//These are the outputs used by the optimizer plus proj_out and E_reg for monitoring.
typedef struct _outputs{
	double * grad;
	double * proj_out;
	double E;
	double E_reg;
}outputs;

//This structure contains various dimensions of the reconstruction and projection for convenience.
typedef struct _dims{
	double nx;
	double ny;
	double nz;
	double * restrict X;
	double * restrict Y;
	double * restrict Z;
	double * restrict dx;
	double * restrict dy;
	int num_segments;
	int num_voxels;
	int num_projections;
	int * restrict projection_size;
	int sum_proj_size;
	int * restrict tomo_size; 
	int * restrict data_shape;
	int * restrict cumsize;
} dims;


//This contains the coefficients for the optimization and the input variables being optimized.
typedef struct _opt_inputs{
	int _3d_kernel_size; //Smoothing kernel
	double * restrict _3d_kernel;
	double * restrict coeff_soft_limits_low;
	double * restrict coeff_soft_limits_high;
	double * restrict soft_limit_weight_coeff;
	double  regularization_angle_coeff;
	double * restrict opt_vector; //All optimization parameters [theta ... ... phi ... ... a ... ...].
						 //Zero pad where unused parameters would be.
} opt_inputs;

//kernel_inputs has the variables that goes into the kernel part of this code, copied
//from opt_inputs->opt_vector and/or s_struct, depending on the optimization settings.
typedef struct _kernel_inputs{
	double * restrict theta_struct; 
	double * restrict phi_struct;
	double * restrict a;
	double * restrict rot_str;
	double * restrict rot_str_diff_theta;
	double * restrict rot_str_diff_phi;
	double * restrict unit_q_beamline;
} kernel_inputs;

//Outputs from the kernel - gradients, errors, synthetic projection.
typedef struct _kernel_outputs{
	double * restrict grad_theta;
	double * restrict grad_phi;
	double * restrict grad_a;
	double E_out;
	double E_reg;
	double * restrict synth_proj;
} kernel_outputs;

//s for spherical harmonics (I think?)

typedef struct _s_struct{
	double * restrict theta; //Values when not optimizing and initial values
	double * restrict phi;	//
	double * restrict a;		//
	int * restrict mask_3d; //integer mask (1, 0, 1, 0...)
	int * restrict window_mask; //integer mask (1, 0, 1, 0...)
	double * restrict mask_3d_d; //Doubles mask (1.0..., 0.0..., 1.0..., 0.0...) needed for some "picky" functions
	int l_max; //Maximum of spherical harmonic parameters
	int m_max;
} s_struct;

//p for parameters, these are functions of the setup.
typedef struct _p_struct{
	int * restrict settings;
	double * restrict phi_det;
	double * restrict theta_det;
	int * restrict opt_coeff; //Whether to optimize (1) for a given coefficient or not (0)
	int num_opt_coeff; //Number of a coefficients
	int num_tot_coeff; //Total number of a coefficients
	dims * restrict dims_in;
	double * restrict rot_exp;
	double * restrict data; //Projection data to be compared with synthetic projection
	double * restrict rot_x;
	double * restrict rot_y;
	double  regularization_scalar_coeff;
} p_struct;

typedef struct _complex{
	double real;
	double imag;
} complex_struct;

/*
	p->settings: 1 or 0 for different options
	p->settings[0] = return_synth_proj;
	p->settings[1] = find_grad;
	p->settings[2] = find_coefficients;
	p->settings[3] = find_orientation;
	p->settings[4] = return_ereg
	p->settings[5] = regularization (sieves)
	p->settings[6] = skip_optimization
	p->settings[7] = avoid_wrapping
	p->settings[8] = scalar_regularization
*/
void _3dsaxs_error_metric_kernel(opt_inputs * , kernel_inputs * , p_struct * , s_struct * , kernel_outputs *);
void _3dsaxs_error_metric(opt_inputs *, p_struct *, s_struct *, outputs * );

#endif