#include <stdio.h>
#include <stdlib.h>
#include <mkl.h>



void main(){
    int xyz = 1;
    printf("test\n");
    printf("test\n");
    printf("test\n");
    int a = 0;
    int status,mode=VSL_CONV_MODE_DIRECT,status2,errcode;
    MKL_INT dims=4;
    VSLConvTaskPtr task;
    //task = (VSLConvTaskPtr*) malloc(sizeof(VSLConvTaskPtr));
    double *kernel;
    kernel = (double*) malloc(sizeof(double)*3*3*3);
    double val[3]={0.25, 0.5,0.25};
    double normer=0;
    for (int j = 0; j < 3; j++)  	
    {for (int k = 0; k < 3; k++)  	
    {for (int l = 0; l < 3; l++)  	
    {normer += val[j]*val[k]*val[l];}}}
    for (int j = 0; j < 3; j++)  	
    {for (int k = 0; k < 3; k++)  	
    {for (int l = 0; l < 3; l++)  	
    {
    	kernel[j*9+k*3+l] = val[j]*val[k]*val[l];

    }}}
    MKL_INT kernel_size=3;

    MKL_INT *kernel_shape;

    kernel_shape = (MKL_INT*) malloc(sizeof(MKL_INT)*4);
    for(int i=0;i<3;i++){
        kernel_shape[i] = kernel_size;
    }
    kernel_shape[3]=1;
    MKL_INT *strides,*strides2,*kernelstrides;

    strides = (MKL_INT*) malloc(sizeof(MKL_INT)*4);
    strides2 = (MKL_INT*) malloc(sizeof(MKL_INT)*4);
    kernelstrides = (MKL_INT*) malloc(sizeof(MKL_INT)*4);
    double *image_in;
    image_in = (double*) malloc(sizeof(double)*8*8*8*2);    
    double val2[8]={0.125,0.25, 0.5, 1.,1,0.5, 0.25,0.125};
    for (int j = 0; j < 8; j++)
    {for (int k = 0; k < 8; k++)
    {for (int l = 0; l < 8; l++)
    {for (int m = 0;m < 2; m++){
    	image_in[j*128+k*16+l*2+m] = val2[j]*val2[k]*val2[l]+3*(m+1);
    }
    }}}
    MKL_INT *image_dims,*image_dims2;
    image_dims = (MKL_INT*) malloc(sizeof(MKL_INT)*4);
    image_dims2 = (MKL_INT*) malloc(sizeof(MKL_INT)*4);
    image_dims[0]=8;
    image_dims[1]=8;
    image_dims[2]=8;
    image_dims[3]=2;
    image_dims2[0]=8;
    image_dims2[1]=8;
    image_dims2[2]=8;
    image_dims2[3]=2;
    printf("test\n");

    double *image_out;
    image_out = (double*) malloc(sizeof(double)*8*8*8*2);
    for (int j = 0; j < 8*8*8*2; j++)
    {
    	image_out[j] = 0.;
    }
    strides[3] = 1;
    strides[2] = 2;
    strides[1] = 16;
    strides[0] = 128;
    
    strides2[3] = 1;
    strides2[2] = 2;
    strides2[1] = 16;
    strides2[0] = 128;
    kernelstrides[3] = 1;
    kernelstrides[2] = 1;
    kernelstrides[1] = 3;
    kernelstrides[0] = 9;
    for (int j = 0; j < 20; j++)
    {
    	printf("%f,  %f\n",image_in[j],image_out[j]);	
    }
    for (int j = 0; j < 9; j++)
    {
    	printf("%f\n",kernel[j]);	
    }
    MKL_INT *dcim;
    dcim = (MKL_INT*)malloc(sizeof(MKL_INT)*4);
    dcim[0] = 1;
    dcim[1] = 1;
    dcim[2] = 1;
    dcim[3] = 0;
    printf("%i, %i\n",kernel_shape[0],kernel_shape[1]);
    status = vsldConvNewTask(&task,mode,dims,image_dims,kernel_shape,image_dims2);
    printf("%i\n",status);   
    printf("%p\n",task);
    status = vslConvSetStart(task,dcim);
    printf("%i\n",status);   
    status2 = vsldConvExec(task,image_in,strides,kernel,kernelstrides,image_out,strides2);
    errcode = vslConvDeleteTask(task);
        printf("%i\n",status2);   
    printf("%i\n",errcode);    
    printf("%i\n",mode);
    for (int m= 0; m < 2; ++m)
    {
    


    for (int j = 0; j < 8; j++)
    {
    	for (int k = 0; k < 8; k++)
    	{
    		for (int l = 0; l < 8; l++)
    		{
    			printf("%f  ",image_in[2*(j*8*8 + k*8 + l)+m]);	
    		}
    		printf("\n");
    	}
    	printf("----------------------------\n");
    }
    printf("=============================\n============================\n");
    }    for (int m= 0; m < 2; ++m)
    {
    

    for (int j = 0; j < 8; j++)
    {
    	for (int k = 0; k < 8; k++)
    	{
    		for (int l = 0; l < 8; l++)
    		{
    			printf("%f  ",image_out[m+2*(j*8*8 + k*8 + l)]);	
    		}
    		printf("\n");
    	}
    	printf("----------------------------\n");
    }
    printf("============================\n============================\n");
}
    printf("%f\n",normer);
	return;
	
}