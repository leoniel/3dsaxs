#include <assert.h>
#include <stdio.h>
#include <string.h>
#include "mkl.h"
#define VSL_STATUS_OK 0
int main()
{
    VSLConvTaskPtr task;
    
    MKL_INT f_shape[] = { 4, 4 };
    MKL_INT g_shape[] = { 4, 4 };
    MKL_INT Rmin[] = { 0, 0 };
    MKL_INT Rmax[] = { f_shape[0] + g_shape[0] - 1, f_shape[1] + g_shape[1] - 1 };

#if 0 /* FULL RESULT */    
    MKL_INT h_shape[] = { Rmax[0], Rmax[1] };
    MKL_INT h_start[] = { 0, 0 };
#elif 1 /* A BOTTOM-LEFT TILE OF THE RESULT */
    MKL_INT size0 = 3;
    MKL_INT size1 = 2;
    MKL_INT h_shape[] = { size0, size1 };
    MKL_INT h_start[] = { Rmax[0]-size0, 0 };
#elif 1 /* A MIDDLE TILE OF THE RESULT */
    MKL_INT size0 = Rmax[0] / 2;
    MKL_INT size1 = Rmax[1] / 2;
    MKL_INT h_shape[] = { size0, size1 };
    MKL_INT h_start[] = { (Rmax[0] - size0) / 2, (Rmax[1] - size1) / 2 };
#endif

    MKL_INT f_stride[] = { f_shape[1], 1 };
    MKL_INT g_stride[] = { g_shape[1], 1 };
    MKL_INT h_stride[] = { h_shape[1], 1 };
    
    double *f = new double[ f_stride[0] * f_shape[0] ];
    double *g = new double[ g_stride[0] * g_shape[0] ];
    double *h = new double[ h_stride[0] * h_shape[0] ];

    for(int i=0; i < f_shape[0]; ++i)
        for(int j=0; j < f_shape[1]; ++j)
            f[ i * f_stride[0] + j ] = 1;

    for(int i=0; i < g_shape[0]; ++i)
        for(int j=0; j < g_shape[1]; ++j)
            g[ i * g_stride[0] + j ] = 1;

    memset( h, 0, sizeof(h[0]) * h_stride[0] * h_shape[0] );

    int status;
    status = vsldConvNewTask( &task, VSL_CONV_MODE_AUTO, 2, f_shape, g_shape, h_shape );
    printf("%i\n",status);
    assert(status == VSL_STATUS_OK);

    status = vslConvSetStart( task, h_start );
    assert(status == VSL_STATUS_OK);

    status = vsldConvExec( task, f, f_stride, g, g_stride, h, h_stride );    
    printf("%i\n",status);
    //assert(status == VSL_STATUS_OK);

    status = vslConvDeleteTask(&task);
    //assert(status == VSL_STATUS_OK);
    
    for (int i=0; i< h_shape[0]; ++i)
    {
        printf("%3i: ",i);
        for (int j=0; j < h_shape[1]; ++j)
        {
            printf("%4.0f ",h[ i * h_stride[0] + j ]);
        }
        printf("\n");
    }
    return 0;
}