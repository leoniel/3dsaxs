# Meeting 2020-08-25

```python
class DataContainer:

    def __init__(self, ...):
        self._data = None
        self._align_parameters = None
        
    def read_from_hdf5(self, filename: str):
        self._data = ...
        # look into https://xarray.pydata.org/
        
    def __repr__(self):
        ...
        
    def __str__(self) -> str:
        return "I am a data container and I hold ... data sets"
    
    def align(self, **kwargs) -> None:
        ...
        self._align_parameters = ...
    
    @property
    def n_projections(self) -> int:
        return len(self._data)


class BaseOptimizer(ABC):

    def __init__(self, ...):
        ...
        
    @abstractmethod
    def train(self, ...):
    
        
class Optimizer(BaseOptimizer):

    ...


# raw data after "online" stage
# * x y (z) : point in sample onto which the beam is focused
# * alpha beta : positioning of sample stage
# * q phi : representation of position in detector image (the 2D detector data is integrated over phi and q)
# --> tens to hundreds of GBytes

dc = DataContainer.from_hdf5('filename.h5')


print(dc)
>> "I am a data container and I am not aligned"

img = dc.get(q=..., alpha=..., beta=..., phi=...)  # type: ndarray
plot.plot(img)

dc.align(kwargs)
print(dc)
>> "I am a data container and I am aligned"

opt = Optimizer(dc.get_data(), fit_method='ridge', ...)
opt.train()
opt.parameters

```

## Next steps

* set up repo (together)
* test driven development based on code snippet above
* computation of objective/loss function in C
* optimizer can (hopefully) be kept on Python side