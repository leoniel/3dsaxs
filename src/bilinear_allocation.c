#include <stdio.h>
#include <stdlib.h>
#include "mkl.h"
#include "cblas.h"
#include <assert.h>
#include <math.h>

/*
Kernel functions for arbitrary projection and arbitrary back projection for tensor tomography.
*/

/*
Projects 3D object onto a 2D map, using bilinear allocation.
*/
 
void allocate(double * restrict proj_out, int * size_out, const double * restrict tomo_obj, int * tallsize, const double * restrict Ax, const double * restrict Ay, const double * restrict Tx, const double * restrict Ty) {
	const int nx = size_out[0];
    const int ny = size_out[1];	
    const int nz = size_out[2]; 
    double t1=0,t2=0,t3=0,t4=0;
    int ind_in=0,ind_out=0;      
    for(int i=0;i<*tallsize;i++){   
        if((Ax[i] > 0.5)&(Ax[i]<nx)&(Ay[i]>0.5)&(Ay[i]<ny)){        

            t1=Tx[i]*Ty[i];
            t2=Tx[i]*(1-Ty[i]);
            t3=(1-Tx[i])*(Ty[i]);
            t4=(1-Tx[i])*(1-Ty[i]);

            ind_out = Ay[i]*nz+ny*Ax[i]*nz-1;
            ind_in = i*nz-1;
            

            for(int j=0;j<nz;j++){ 
                // printf("ind out: %d\n",ind_out);
                // printf("ind in: %d\n",ind_in);
                // printf("t: %e, %e, %e, %e",t1,t2,t3,t4);
                ind_in += 1;
                ind_out += 1;   
                proj_out[ind_out] += tomo_obj[ind_in]*t1;
                proj_out[ind_out-nz] += tomo_obj[ind_in]*t2;
                proj_out[ind_out-ny*nz] += tomo_obj[ind_in]*t3;
                proj_out[ind_out-ny*nz-nz] += tomo_obj[ind_in]*t4;

            }
        }
        
    }
    //printf("Proj_upsample: %f\n",proj_out[95]);
    return;
}

/*
Upsamples 3D object, potentially with 4th dimension for tensor components.
*/

void upsample(double * p_out, const double * restrict in_obj, const int * dims_in, const int * dims_size, const int * uval) {
	const int nX = dims_in[0]*2;
    const int nY = dims_in[1]*2;	
    const int nZ = dims_in[2]*2;
    int temp = 1;
    if (*dims_size > 3){
        temp = dims_in[3];
    }
    const int nQ = temp;
    int in_ind = 0;
    int out_ind=0;
    int shift1,shift2,shift3;
    double val;
    int j=1,k=1,l=1,i=0;
    //printf("Which one?\n");
    //     printf("%d\n\n",dims_in[0]*dims_in[1]*dims_in[2]*dims_in[3]);
    //     printf("%f\n\n",in_obj[64896]);
    // printf("Invalid?\n");
    // printf("Ẁell?\n");
    // printf("Is it?\n");
    for (j = 1; j < nX; j+=*uval) { 
        shift1=j*nY*nZ*nQ;
        for (k = 1; k < nY; k+=*uval) { 
            shift2=shift1+k*nZ*nQ;
            for (l = 1; l < nZ; l+=*uval) { 
                shift3=shift2+l*nQ;
                for (i=0;i<nQ; i++){
                     // if (in_ind >75410){
                     // printf("in ind: %d\n",in_ind);
                     // printf("ptr: %p\n",&in_obj[in_ind]);
                     // printf("ptr: %p\n",&p_out[680808]);
                     // printf("ptr: %f\n",p_out[680808]);
                     // printf("max out ind: %d\n",out_ind+nQ*((nZ)*(nY))+nZ*nQ+nQ);}
                    out_ind=shift3+i;
                    val = in_obj[in_ind++];
                    p_out[out_ind] = val;
                    p_out[out_ind-nQ] += val*0.5;
                    p_out[out_ind+nQ] += val*0.5;
                    p_out[out_ind-nQ*(nZ)] += val*0.5;
                    p_out[out_ind+nQ*(nZ)] += val*0.5;
                    p_out[out_ind-nQ*(nZ*nY)] += val*0.5;
                    p_out[out_ind+nQ*(nZ*nY)] += val*0.5;
                    p_out[out_ind-(nQ)*(nZ)+nQ] += val*0.25;
                    p_out[out_ind-(nQ)*(nZ)-nQ] += val*0.25;
                    p_out[out_ind+(nQ)*(nZ)-nQ] += val*0.25;
                    p_out[out_ind+(nQ)*(nZ)+nQ] += val*0.25;
                    p_out[out_ind-nQ*(nZ*(nY))-nQ] += val*0.25;
                    p_out[out_ind+nQ*(nZ*(nY))+nQ] += val*0.25;
                    p_out[out_ind-nQ*(nZ*(nY))+nQ] += val*0.25;
                    p_out[out_ind+nQ*(nZ*(nY))-nQ] += val*0.25;
                    p_out[out_ind-nQ*((nZ)*(nY))-nZ*nQ] += val*0.25;
                    p_out[out_ind+nQ*((nZ)*(nY))+nZ*nQ] += val*0.25;
                    p_out[out_ind-nQ*((nZ)*(nY))+nZ*nQ] += val*0.25;
                    p_out[out_ind+nQ*((nZ)*(nY))-nZ*nQ] += val*0.25;
                    p_out[out_ind-nQ*((nZ)*(nY))-nZ*nQ-nQ] += val*0.125;
                    p_out[out_ind+nQ*((nZ)*(nY))+nZ*nQ-nQ] += val*0.125;
                    p_out[out_ind-nQ*((nZ)*(nY))+nZ*nQ-nQ] += val*0.125;
                    p_out[out_ind+nQ*((nZ)*(nY))-nZ*nQ-nQ] += val*0.125;
                    p_out[out_ind-nQ*((nZ)*(nY))-nZ*nQ+nQ] += val*0.125;
                    p_out[out_ind+nQ*((nZ)*(nY))+nZ*nQ+nQ] += val*0.125;
                    p_out[out_ind-nQ*((nZ)*(nY))+nZ*nQ+nQ] += val*0.125;
                    p_out[out_ind+nQ*((nZ)*(nY))-nZ*nQ+nQ] += val*0.125;
                }
            }
        }
    }
    
    return;
}

/*Back-projects 2D NX, NY image into 3D nx*ny*nz space specified by 2D coordinates Ax, Ay of size nx*ny*nz,
effectively specifying the back-projection angle. Tx, Ty interpolates bilinearly to reduce aliasing.*/

void allocate_back(double * restrict tomo_out, int * size_out,int * size_in, const double * restrict proj_obj, int * tallsize,  const double * restrict Ax, const double * restrict Ay,  const double * restrict Tx, const double * restrict Ty) {
	int nx = size_in[0];
    int ny = size_in[1];
    int nz = size_out[2];
    int nq = size_out[3];
    double t1,t2,t3,t4;
    int ind_in=0,ind_out=0;        
    for(int i=0;i<*tallsize;i++){   
            
        if((Ax[i] > 0.5)&(Ax[i]<nx)&(Ay[i]>0.5)&(Ay[i]<ny)){
            ind_in = (Ay[i])*nq+(Ax[i])*nq*ny-1;
            ind_out = i*nq-1;
            t1=Tx[i]*Ty[i];
            t2=Tx[i]*(1-Ty[i]);
            t3=(1-Tx[i])*(Ty[i]);
            t4=(1-Tx[i])*(1-Ty[i]);

            for(int j=0;j<nq;j++){

                // printf("ind out: %d\n",ind_out);
                // printf("ind in: %d\n",ind_in);
                // printf("t: %e, %e, %e, %e\n",t1,t2,t3,t4);
                ind_in += 1;
                ind_out += 1;            
                tomo_out[ind_out] = proj_obj[ind_in]*t1+proj_obj[ind_in-nq]*t2+ proj_obj[ind_in-ny*nq]*t3+ proj_obj[ind_in-ny*nq-nq]*t4;


                
            
            }
        }
    }
}

// void convolve_downsample_2d(double * image_out, double * image_in, double * kernel, int * image_dims, int  *kernel_size){

//     double * toeplitz1,* toeplitz2;
//     toeplitz1 = (double*) malloc(sizeof(double)*((image_dims[0]+*kernel_size-1)*image_dims[0]));
//     toeplitz2 = (double*) malloc(sizeof(double)*((image_dims[1]+*kernel_size-1)*image_dims[1]));


//     int i,j;

//     for(i=0;i<image_dims[0];i++){
//         for(j=0;j<image_dims[0]+*kernel_size-1;j++){
//             if((i+*kernel_size>j)&&(j>=i)){
//                 toeplitz1[i*(image_dims[0]+*kernel_size-1)+j] = kernel[j-i];
//             } else {
//                 toeplitz1[i*(image_dims[0]+*kernel_size-1)+j] = 0;

//             }
//         }
//     }    

//     CBLAS_LAYOUT Layout;
//     CBLAS_TRANSPOSE transa;
//     CBLAS_TRANSPOSE transb;

//     Layout = CblasRowMajor;
//     transa = CblasNoTrans;
//     transb = CblasNoTrans;

//     double * zero_pad;
//     zero_pad = (double*) malloc(sizeof(double)*(image_dims[0]+*kernel_size-1)*(image_dims[1]+*kernel_size-1));
//     for (i=0;i<(image_dims[0]+*kernel_size-1);i++){
//         for (j=0;j<(image_dims[1]+*kernel_size-1);j++){
//             if ((i<(*kernel_size-1)/2)||(j<(*kernel_size-1)/2)||(image_dims[0]-i<-1)||(image_dims[1]-j<-1)){
//                 zero_pad[(image_dims[1]+*kernel_size-1)*i+j]=0;
//             } else {
//                 zero_pad[(image_dims[1]+*kernel_size-1)*i+j]=image_in[(image_dims[1])*(i-(*kernel_size-1)/2)+j-(*kernel_size-1)/2];
//             }
//         }
           
//     }

//     double * zero_pad_out;
//     zero_pad_out = (double*) malloc(sizeof(double)*((image_dims[1]+*kernel_size-1)*(image_dims[0])+1));
//     int rows = image_dims[0],cols=image_dims[1]+*kernel_size-1,cols2=image_dims[0]+*kernel_size-1;  

//     for (i = 0; i < (image_dims[1]+*kernel_size-1)*(image_dims[0]); ++i)
//     {
//            zero_pad_out[i]=0;
//     }   

//     cblas_dgemm(Layout,transa,transb,rows,cols,cols2,1,toeplitz1,cols2,zero_pad,cols,0,zero_pad_out,cols);    

//     for(i=0;i<image_dims[1];i++){
//         for(j=0;j<image_dims[1]+*kernel_size-1;j++){
//             if((i+*kernel_size>j)&&(j>=i)){
//                 toeplitz2[i*(image_dims[1]+*kernel_size-1)+j] = kernel[j-i];
//             } else {
//                 toeplitz2[i*(image_dims[1]+*kernel_size-1)+j] = 0;
//             }
//         }
//     }

//     rows = image_dims[1];
//     cols = image_dims[0];
//     cols2 = image_dims[1]+*kernel_size-1;
//     double * temp_in;
//     temp_in= (double*) malloc(sizeof(double)*image_dims[0]*(image_dims[1]));

//     for (i = 0; i < image_dims[0]*image_dims[1]; i++){
//         temp_in[i]=0;
//     }

//     cblas_dgemm(Layout,transa,transb,rows,cols,cols2,1,toeplitz2,cols2,zero_pad_out,cols2,0,temp_in,cols);

//     double * downsampler, * downsampler2, * temp_out;
//     downsampler = (double*) malloc(sizeof(double)*image_dims[1]*((image_dims[1]-1)/2));
//     downsampler2 = (double*) malloc(sizeof(double)*image_dims[0]*((image_dims[0]-1)/2));
//     temp_out= (double*) malloc(sizeof(double)*image_dims[0]*((image_dims[1]-1)/2));

//     for (i = 0; i < image_dims[0]*((image_dims[1]-1)/2); i++){
//         temp_out[i]=0;
//     }

//     for (i = 0; i < (image_dims[1]-1)/2; i++)
//     {
//         for (j = 0; j< image_dims[1]; j++)
//         {
//             if((2*i==j)||(j-2==2*i)){
//                 downsampler[i*image_dims[1]+j] = 0.125;
//             } else if(j-1==2*i){
//                 downsampler[i*image_dims[1]+j] = 0.25;
//             } else {
//               downsampler[i*image_dims[1]+j]=0;  
//             } 
//         }
//     }    

//     for (i = 0; i < (image_dims[0]-1)/2; i++)
//     {
//         for (j = 0; j< image_dims[0]; j++)
//         {
//             if((2*i==j)||(j-2==2*i)){
//                 downsampler2[i*image_dims[0]+j] = 0.125;
//             } else if(j-1==2*i){
//                 downsampler2[i*image_dims[0]+j] = 0.25;
//             } else {
//               downsampler2[i*image_dims[0]+j]=0;  
//             } 
//         }
//     }   

//     transb = CblasNoTrans;
//     rows = (image_dims[1]-1)/2;
//     cols = image_dims[0];
//     cols2 = image_dims[1];
//         for (j = 0; j < rows*cols; j++)
//     {   if (j<100)
// {        printf("j: %d, imoutless: %f\n", j, downsampler[j]);
// }
//     cblas_dgemm(Layout,transa,transb,rows,cols,cols2,1,downsampler,cols2,image_in,cols,0,temp_out,cols);    

//     transb = CblasTrans;
//     rows = (image_dims[0]-1)/2;
//     cols = (image_dims[1]-1)/2;
//     cols2 = image_dims[0];    
//     cblas_dgemm(Layout,transa,transb,rows,cols,cols2,1,downsampler2,cols2,temp_out,cols2,0,image_out,cols);

//     }

//     free(toeplitz1);
//     free(toeplitz2);
//     free(zero_pad);
//     free(zero_pad_out);
//     free(downsampler);
//     free(downsampler2);
//     free(temp_out);
//     free(temp_in);

//     return;
// }

void convolve_3d(double * image_out, double * image_in, double * kernel, int * image_dims, int  kernel_size){
    int status,mode=VSL_CONV_MODE_DIRECT,status2,errcode;
    MKL_INT dims=4;
    VSLConvTaskPtr *task;
    task = calloc(1,sizeof(VSLConvTaskPtr));
    int kernel_shape[4];
    kernel_shape[2] = kernel_size;
    kernel_shape[1] = kernel_size;
    kernel_shape[0] = kernel_size;
    kernel_shape[3] = 1;
    MKL_INT strides[4],kernelstrides[4];    

    strides[3] = 1;
    strides[2] = image_dims[3];
    strides[1] = image_dims[2]*image_dims[3];
    strides[0] = image_dims[3]*image_dims[2]*image_dims[1];
    kernelstrides[3] = 1;
    kernelstrides[2] = kernel_shape[3];
    kernelstrides[1] = kernel_shape[3]*kernel_shape[2];
    kernelstrides[0] = kernel_shape[3]*kernel_shape[2]*kernel_shape[1];
    MKL_INT start[4] = {(kernel_size-1)/2,(kernel_size-1)/2,(kernel_size-1)/2,0};
    //MKL_INT start[4] = {0,0,0,0};
    status = vsldConvNewTask(task,mode,dims,image_dims,kernel_shape,image_dims);
    assert(status==0);
    status = vslConvSetStart(*task,start);
    assert(status==0);
    status2 = vsldConvExec(*task,image_in,strides,kernel,kernelstrides,image_out,strides);
    //printf("Status 2: %d\n",status2);
    assert(status2==0);
    errcode = vslConvDeleteTask(task);
    assert(errcode==0);
    free(task);
    return;
}


void convolve_downsample_2d(double * image_out, double * image_in, double * kernel, int * image_dims, int  kernel_size){
    int status,mode=VSL_CONV_MODE_DIRECT,status2,errcode;
    MKL_INT dims=3;
    VSLConvTaskPtr *task;
    task = calloc(1,sizeof(VSLConvTaskPtr));
    int kernel_shape[3],decimation[3];
    kernel_shape[2] = 1;
    kernel_shape[1] = kernel_size;
    kernel_shape[0] = kernel_size;
    MKL_INT strides[3],kernelstrides[3],stridesin[3];   
    //printf("%d\n\n\n",image_dims[1]); 
    int image_dims_out[3] = {floorf((image_dims[0])/2),floorf((image_dims[1])/2),floorf(image_dims[2])};
    int image_dims_now[3] = {(image_dims[0]),(image_dims[1]),image_dims[2]};
    strides[2] = 1;
    strides[1] = (image_dims_out[2]);
    strides[0] = (image_dims_out[2]*image_dims_out[1]);    
    stridesin[2] = 1;
    stridesin[1] = (image_dims[2]);
    stridesin[0] = (image_dims[2]*image_dims[1]);

    kernelstrides[2] = 1;
    kernelstrides[1] = kernel_shape[2];
    kernelstrides[0] = kernel_shape[2]*kernel_shape[1];
    decimation[0] = 2;
    decimation[1] = 2;
    decimation[2] = 1;
    //MKL_INT start[3] = {0,0,0};
    MKL_INT start[3] = {(kernel_size-1)/2+1,(kernel_size-1)/2+1,0};
    // for (int i = 1000; i < 1040; ++i)
    // {
    //     printf("in: %e\n", image_in[i]);
    // }
    // printf("%d\n%d\n%d\n", image_dims[0],image_dims[1],image_dims[2]);
    // printf("%d\n%d\n%d\n", image_dims_out[0],image_dims_out[1],image_dims_out[2]);
    //printf("%d\n%d\n%d\n", stridesin[0],stridesin[1],stridesin[2]);
    double *im1,*im2,*im3,*im4;
    im1 = (double*) calloc(image_dims_out[0]*image_dims_out[1]*image_dims_out[2],sizeof(double));
    im2 = (double*) calloc(image_dims_out[0]*image_dims_out[1]*image_dims_out[2],sizeof(double));
    im3 = (double*) calloc(image_dims_out[0]*image_dims_out[1]*image_dims_out[2],sizeof(double));
    im4 = (double*) calloc(image_dims_out[0]*image_dims_out[1]*image_dims_out[2],sizeof(double));
    status = vsldConvNewTask(task,mode,dims,image_dims_now,kernel_shape,image_dims_out);
    assert(status==0);
    status = vslConvSetStart(*task,start);
    status = vslConvSetDecimation(*task,decimation);
    assert(status==0);
    status2 = vsldConvExec(*task,image_in,stridesin,kernel,kernelstrides,im1,strides);
    //printf("Status 2: %d\n",status2);    
    //printf("imdims: %d, %d, %d\n",image_dims_out[0],image_dims_out[1],image_dims_out[2]);
    assert(status2==0);
    MKL_INT start2[3] = {(kernel_size-1)/2,(kernel_size-1)/2,0};
    status = vslConvSetStart(*task,start2);
    status = vslConvSetDecimation(*task,decimation);
    assert(status==0);
    status2 = vsldConvExec(*task,image_in,stridesin,kernel,kernelstrides,im2,strides);
    MKL_INT start3[3] = {(kernel_size-1)/2+1,(kernel_size-1)/2,0};
    status = vslConvSetStart(*task,start3);
    status = vslConvSetDecimation(*task,decimation);
    assert(status==0);
    status2 = vsldConvExec(*task,image_in,stridesin,kernel,kernelstrides,im3,strides);
    MKL_INT start4[3] = {(kernel_size-1)/2,(kernel_size-1)/2+1,0};
    status = vslConvSetStart(*task,start4);
    status = vslConvSetDecimation(*task,decimation);
    assert(status==0);
    status2 = vsldConvExec(*task,image_in,stridesin,kernel,kernelstrides,im4,strides);
    //printf("Status 2: %d\n",status2);    
    //printf("imdims: %d, %d, %d\n",image_dims_out[0],image_dims_out[1],image_dims_out[2]);
    assert(status2==0);
    errcode = vslConvDeleteTask(task);
    vdAdd(image_dims_out[0]*image_dims_out[1]*image_dims_out[2],im1,im2,image_out);  
    vdAdd(image_dims_out[0]*image_dims_out[1]*image_dims_out[2],im3,image_out,image_out);
    vdAdd(image_dims_out[0]*image_dims_out[1]*image_dims_out[2],im4,image_out,image_out);   
    cblas_dscal(image_dims_out[0]*image_dims_out[1]*image_dims_out[2],0.25,image_out,1);
    // assert(errcode==0);
    //     for (int i = 250; i < 260; ++i)
    // {
    //     printf("out: %e\n", image_out[i]);
    // }
    free(task);
    free(im1);
    free(im2);
    free(im3);
    free(im4);
    return;
}