#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include "mkl.h"
#include "cblas.h"
#include <math.h>
#include "bilinear_allocation.h"
#include "angle_regularization.h"
#include "arb_projection.h"
#include "error_metric_kernel.h"
#include "mat.h"
#include "matrix.h"


void cgmin(int itmax,opt_inputs *opt_in, p_struct *p, s_struct *s){
   //VSLStreamStatePtr stream;
    
   //vslNewStream( &stream, VSL_BRNG_MT19937, 777 );
    
   /* Generating */        
	outputs * op;
	op = malloc(sizeof(outputs));
	int num_voxels = p->dims_in->num_voxels;
	int num_coeff = p->num_opt_coeff;
	double E_old[10];
	int ntot = (2+num_coeff)*num_voxels;
	op->grad = (double*) calloc((2+num_coeff)*num_voxels,sizeof(double));
	double * pv;
	pv = (double*) calloc((2+num_coeff)*num_voxels,sizeof(double));
	double * pv_old;
	pv_old = (double*) malloc(sizeof(double)*(2+num_coeff)*num_voxels);
	double * grad_old;
	grad_old = (double*) malloc(sizeof(double)*(2+num_coeff)*num_voxels);

	double r[(2+num_coeff)*num_voxels]; /* buffer for random numbers */
	double * x;
	x = (double*) malloc(sizeof(double)*(2+num_coeff)*num_voxels);


	// for (int i = 0; i < (2*num_coeff)*num_voxels; ++i)
	// {
	// 	op->grad[i] =0;
	// }

	op->proj_out = (double*) malloc(sizeof(double)*p->dims_in->sum_proj_size*p->dims_in->num_segments);

	// for (int i = 0; i < p->dims_in->num_segments*p->dims_in->sum_proj_size; ++i)
	// {
	// 	op->proj_out[i] = 0;
	// }

	op->E = 0;
	op->E_reg = 0;

	double norm_old,norm_new,beta;
	double stepsize = 20,step;
	int min_ind;
	// vdRngGaussian( VSL_RNG_METHOD_CAUCHY_ICDF, stream, (2+num_coeff)*num_voxels, r, 0.0, sqrt(stepsize)*1000.0 );
	
	// vdMul(ntot,r,opt_in->opt_vector,r);
	// vdAdd(ntot,r,opt_in->opt_vector,opt_in->opt_vector); 
	_3dsaxs_error_metric(opt_in, p, s, op);
	//First step is a "gradient step"
	cblas_dcopy(ntot,op->grad,1,pv,1);
	cblas_dscal(ntot,-1./cblas_dnrm2(ntot,op->grad,1),pv,1);
	cblas_dcopy(ntot,pv,1,pv_old,1);
	cblas_dcopy(ntot,opt_in->opt_vector,1,x,1);

	norm_old = cblas_dnrm2(ntot,op->grad,1);
	norm_new = norm_old;
	cblas_dcopy(ntot,op->grad,1,grad_old,1);
	
	printf("Norm: %f\n", cblas_dnrm2(ntot,op->grad,1));

	// double e_min;
	// for (int i = 0; i < 1; ++i)
	// {			

	// 	step = pow(0.5+i,1.)*stepsize;
	// 	cblas_dcopy(ntot,pv_old,1,pv,1);		
	// 	cblas_dscal(ntot,step,pv,1);
	// 	vdAdd(ntot,x,pv,opt_in->opt_vector);
	
	// 	_3dsaxs_error_metric(opt_in, p, s, op);
	// 	E_old[i] = op->E;
	// }
	// e_min = E_old[0];
	// min_ind=0;
	// for (int i = 1; i < 1; ++i)
	// {
	// 	if (E_old[i] < e_min){
	// 		e_min = E_old[i];
	// 		min_ind=i;
	// 	} 
	// }
	// 	stepsize = pow(0.5+min_ind*0.5,1.)*stepsize;

	cblas_daxpy(ntot,stepsize,pv_old,1,x,1);
	cblas_dcopy(ntot,x,1,opt_in->opt_vector,1);
	stepsize*=0.5;
	//Remaining steps add a second-order term to the added gradient.
	double * diff;
	double df,f,f2,f1,a1,f0;
	diff = (double*) calloc(ntot,sizeof(double));
	double dot;
	int success=0;
	for (int j = 0; j < itmax; ++j)
	{
		stepsize = fmax(stepsize,0.00001);
		_3dsaxs_error_metric(opt_in, p, s,op);		

		printf("Iteration: %d \n \n", j);
		printf("Stepsize: %f \n \n", stepsize);

		// printf("Error: %f \n Regularization error = %f \n", op->E,op->E_reg);
		f0 = op->E;
		f=f0;
		cblas_dcopy(ntot,op->grad,1,pv,1);

		norm_old = norm_new;
		norm_new = cblas_dnrm2(ntot,op->grad,1);
		vdSub(ntot,op->grad,grad_old,diff);
		dot = cblas_ddot(ntot,op->grad,1,diff,1);


		beta = (dot)/(norm_old*norm_old);
		printf("norm old: %f\n", norm_old);

		printf("norm: %f\n", norm_new);
		printf("beta: %f\n", beta);
		printf("dot: %f\n",dot);
		printf("nrmdiff: %f\n",cblas_dnrm2(ntot,diff,1));
		cblas_dscal(ntot,-1,pv,1.);
		if (success&(j%8!=0)){
		cblas_daxpy(ntot,beta,pv_old,1,pv,1);    	 
		}
		if ((norm_new<1e-2)&(j>5)&(stepsize<1e-2))
		{
			printf("Gradient is too small...\n");
			break;
		}
		cblas_dscal(ntot,1./cblas_dnrm2(ntot,pv,1),pv,1.);

		cblas_dcopy(ntot,pv,1,pv_old,1);
		cblas_dcopy(ntot,op->grad,1,grad_old,1);
		cblas_dcopy(ntot,opt_in->opt_vector,1,x,1);

		df = cblas_ddot(ntot,pv,1,op->grad,1);
		cblas_dcopy(ntot,pv_old,1,pv,1);		
		cblas_dscal(ntot,stepsize,pv,1);
		vdAdd(ntot,x,pv,opt_in->opt_vector);

		p->settings[1] = 0;
		step=stepsize;
		f1=f0;
		_3dsaxs_error_metric(opt_in, p, s, op);
		f = op->E;
		f2=f;
			int counter = 0;
		if (f>f0){

		while (f>f0)
		{			

			printf("\nDecreasing steplength iteration %d...\n\n", counter);
		
			if(f<=f2){
				step=stepsize;
				f2=f;
			}
			stepsize = 0.25*stepsize;
			
			cblas_dcopy(ntot,pv_old,1,pv,1);		
			cblas_dscal(ntot,stepsize,pv,1);
			vdAdd(ntot,x,pv,opt_in->opt_vector);
			_3dsaxs_error_metric(opt_in, p, s, op);
			f = op->E;
			counter++;
			if (counter>4){
				break;
			}
		}
		}
		counter=0;
		while (f2<=f)
		{			
			printf("\nIncreasing steplength iteration %d...\n\n", counter);
			step = 2*step;
			cblas_dcopy(ntot,pv_old,1,pv,1);		
			cblas_dscal(ntot,step,pv,1);
			vdAdd(ntot,x,pv,opt_in->opt_vector);
		
			_3dsaxs_error_metric(opt_in, p, s, op);

			f2 = op->E;
			if(f2<f){
				a1=stepsize;
				stepsize=step;
				f1=f;
				f=f2;
			}
			counter++;

			if (counter>8.){
				break;
			}

		}
		if ((f<f1)&(f<=f2)){
			printf("\n Bracketing success!\n\n");

			success=1;
		} else {
			printf("\n Bracketing failed!\n\n");
			success=0;
		}
		
		cblas_daxpy(ntot,stepsize,pv_old,1,x,1);
		cblas_dcopy(ntot,x,1,opt_in->opt_vector,1);
		stepsize*=1;
		p->settings[1]=1;

	}
	free(pv);
	free(pv_old);
	free(op->grad);
	free(op->proj_out);
	free(x);
	free(grad_old);
	free(diff);
	return;
}


int main(){
	//printf("%d\n",mkl_get_max_threads());

	MATFile * M;
	M = matOpen("saved_cstruct.mat","r");
	opt_inputs *opt_in;
	opt_in = malloc(sizeof(opt_inputs));
	p_struct *p; 
	p = malloc(sizeof(p_struct));
	p->dims_in = malloc(sizeof(dims));
	s_struct *s; 
	s = malloc(sizeof(s_struct));
	p->dims_in->nx = *(double*)mxGetData(matGetVariable(M,"nx"));
	p->dims_in->ny = *(double*)mxGetData(matGetVariable(M,"ny"));
	p->dims_in->nz = *(double*)mxGetData(matGetVariable(M,"nz"));
	p->dims_in->X = (double*)mxGetData(matGetVariable(M,"X"));
	p->dims_in->Y = (double*)mxGetData(matGetVariable(M,"Y"));
	p->dims_in->Z = (double*)mxGetData(matGetVariable(M,"Z"));
	p->dims_in->dx = (double*)mxGetData(matGetVariable(M,"dx"));
	p->dims_in->dy = (double*)mxGetData(matGetVariable(M,"dy"));

	p->dims_in->num_segments = *(int*)mxGetData(matGetVariable(M,"num_segments"));
	p->dims_in->num_voxels = *(int*)mxGetData(matGetVariable(M,"num_voxels"));	
	p->dims_in->num_projections = *(int*)mxGetData(matGetVariable(M,"num_projections"));	
	p->dims_in->projection_size = (int*)mxGetData(matGetVariable(M,"projection_size"));
	p->dims_in->sum_proj_size= *(int*)mxGetData(matGetVariable(M,"sum_projection_size"));
	p->dims_in->tomo_size = (int*)mxGetData(matGetVariable(M,"tomo_size"));
	p->dims_in->data_shape = (int*)mxGetData(matGetVariable(M,"data_shape"));
	//Cumulative projection size, projection i+1 at index cumsize[i]*num_segments
	p->dims_in->cumsize = (int*)mxGetData(matGetVariable(M,"cumsize"));

	p->settings = (int*) mxGetData(matGetVariable(M,"settings"));
	p->phi_det = (double*) mxGetData(matGetVariable(M,"phi_det"));
	p->theta_det = (double*) mxGetData(matGetVariable(M,"theta_det"));
	p->opt_coeff = (int*) mxGetData(matGetVariable(M,"opt_coeff"));
	p->num_opt_coeff = *(int*) mxGetData(matGetVariable(M,"num_opt_coeff"));
	p->rot_exp = (double*) mxGetData(matGetVariable(M,"rot_exp"));
	p->data = (double*) mxGetData(matGetVariable(M,"data"));
	p->rot_x = (double*) mxGetData(matGetVariable(M,"rot_x"));
	p->rot_y = (double*) mxGetData(matGetVariable(M,"rot_y"));
	
	s->theta = (double*) mxGetData(matGetVariable(M,"theta"));
	s->phi = (double*) mxGetData(matGetVariable(M,"phi"));
	s->a = (double*) mxGetData(matGetVariable(M,"a"));
	s->window_mask = (int*) mxGetData(matGetVariable(M,"window_mask"));
	s->mask_3d = (int*) mxGetData(matGetVariable(M,"mask_3d"));
	s->mask_3d_d = (double*) mxGetData(matGetVariable(M,"mask_3d_d"));
	s->l_max = (*(int*) mxGetData(matGetVariable(M,"l_max")));
	s->m_max = (*(int*) mxGetData(matGetVariable(M,"m_max")));


	opt_in->_3d_kernel_size = *(int*) mxGetData(matGetVariable(M,"td_kernel_size"));
	opt_in->_3d_kernel = (double*) mxGetData(matGetVariable(M,"td_kernel"));
	opt_in->coeff_soft_limits_low = (double*) mxGetData(matGetVariable(M,"coeff_soft_limits_low"));
	opt_in->coeff_soft_limits_high = (double*) mxGetData(matGetVariable(M,"coeff_soft_limits_high"));
	opt_in->soft_limit_weight_coeff = (double*) mxGetData(matGetVariable(M,"soft_limit_weight_coeff"));
	opt_in->regularization_angle_coeff = *(double*) mxGetData(matGetVariable(M,"regularization_angle_coeff"));
	opt_in->opt_vector = (double*) mxGetData(matGetVariable(M,"opt_vector"));
	matClose(M);

	int settings[9] = {p->settings[0],p->settings[1],p->settings[2],p->settings[3],p->settings[4],p->settings[5],p->settings[6],p->settings[7],1};
	settings[8]=1;

	p->settings=settings;
	p->regularization_scalar_coeff = 1e-3;
	int itmax=10;
	p->num_tot_coeff = 1;
	cgmin(itmax,opt_in, p, s);

	for (int i = 0; i < p->dims_in->num_voxels; ++i)
	{
		s->theta[i] = opt_in->opt_vector[i];
	}

	for (int i = 0; i < p->dims_in->num_voxels; ++i)
	{	
		// opt_in->opt_vector[p->dims_in->num_voxels + i] = 0.32513;
		s->phi[i] = opt_in->opt_vector[p->dims_in->num_voxels + i];

	}

	for (int i = 0; i < p->dims_in->num_voxels;++i)
		{
			for (int j = 0; j<p->num_opt_coeff; ++j)
	
		{
			if (p->opt_coeff[j]){
			s->a[i*p->num_opt_coeff+j] = opt_in->opt_vector[p->dims_in->num_voxels*2 + i*p->num_opt_coeff+j];
			}
		}
	}
	p->settings[3] = 1;
	p->settings[2] = 1;
	p->settings[5] = 1;
	p->settings[7] = 1;
	int oco[9] = {1,1,1,1,1,1,0,1,1};
	p->opt_coeff = oco;
	p->num_opt_coeff = 6;
	p->num_tot_coeff = 6;
	s->l_max = 2;
	s->m_max = 2;
	double ratios[9] = {1,0,0,0,0,0,0,0,0};
	s->a = calloc(p->dims_in->num_voxels*p->num_opt_coeff,sizeof(double));
	for (int i = 0; i < p->dims_in->num_voxels;++i)
		{
			for (int j = 0; j<p->num_opt_coeff; ++j)
	
		{
			s->a[i*p->num_opt_coeff+j] = opt_in->opt_vector[p->dims_in->num_voxels*2 +i]*ratios[j];
			
		}
	}
	opt_in->opt_vector = calloc(p->dims_in->num_voxels*(2+p->num_opt_coeff),sizeof(double));
	for (int i = 0; i < p->dims_in->num_voxels;++i)
		{
			for (int j = 0; j<p->num_opt_coeff; ++j)
	
		{
			if (p->opt_coeff[j]){
			opt_in->opt_vector[2*p->dims_in->num_voxels+i*p->num_opt_coeff+j] = s->a[i*p->num_opt_coeff+j];
			}
		}
	}

	opt_in->regularization_angle_coeff = 1e-5;
	itmax=500;
	cgmin(itmax,opt_in, p, s);

	free(s->a);
	free(opt_in->opt_vector);
	return 0;
}