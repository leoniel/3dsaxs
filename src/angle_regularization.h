#ifndef _angle_regularization_h
#define _angle_regularization_h


void angle_regularization(const double * restrict, const double * restrict, double * restrict, double * restrict, double * restrict, int, int, int, int);
void scalar_regularization(const double * restrict, double * restrict, double * restrict,int, int, int,int);

#endif