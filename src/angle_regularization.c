#include <stdio.h>
#include <stdlib.h>
#include "mkl.h"
#include "cblas.h"

void angle_regularization(const double * restrict theta_struct, const double * restrict phi_struct, double * restrict grad_theta_reg, double * restrict grad_phi_reg,
 double * restrict regout,int nx, int ny, int nz,int grad){
	double * sin_th, * cos_th, * sin_ph, *cos_ph, *x, *y, *z,*drdx,*drdy,*drdz,*reg;
	sin_th = (double *) calloc(nx*ny*nz,sizeof(double));
	cos_th = (double *) calloc(nx*ny*nz,sizeof(double));
	sin_ph = (double *) calloc(nx*ny*nz,sizeof(double));
	cos_ph = (double *) calloc(nx*ny*nz,sizeof(double));
	reg = (double *) calloc(nx*ny*nz,sizeof(double));
	
	x = (double *) calloc(nx*ny*nz,sizeof(double));
	y = (double *) calloc(nx*ny*nz,sizeof(double));

	drdx = (double *) calloc(nx*ny*nz,sizeof(double));
	drdy = (double *) calloc(nx*ny*nz,sizeof(double));
	drdz = (double *) calloc(nx*ny*nz,sizeof(double));

	int ntot = nx*ny*nz;

	vdSin(ntot,theta_struct,sin_th);
	vdCos(ntot,theta_struct,cos_th);
	vdSin(ntot,phi_struct,sin_ph);
	vdCos(ntot,phi_struct,cos_ph);
	
	vdMul(ntot,sin_th,cos_ph,x);
	vdMul(ntot,sin_th,sin_ph,y);

	z=cos_th;

	int i,j,k;
	reg[0]=0;
	reg[ntot-1]=0;
	drdx[0]=0;
	drdx[ntot-1]=0;
	drdy[0]=0;
	drdy[ntot-1]=0;
	drdz[0]=0;
	drdz[ntot-1]=0;

	for (i = 1; i < nx-1; i++)
	{
		for (j = 1; j < ny-1; j++)
		{
			for (k = 1; k < nz-1; k++)
			{
				reg[i*ny*nz + j*nz + k] += 1-abs(x[i*ny*nz + j*nz + k]*x[i*ny*nz + j*nz + k+1]+y[i*ny*nz + j*nz + k]*y[i*ny*nz + j*nz + k+1]+z[i*ny*nz + j*nz + k]*z[i*ny*nz + j*nz + k+1]);
				reg[i*ny*nz + j*nz + k] += 1-abs(x[i*ny*nz + j*nz + k]*x[i*ny*nz + (j+1)*nz + k]+y[i*ny*nz + j*nz + k]*y[i*ny*nz + (j+1)*nz + k]+z[i*ny*nz + j*nz + k]*z[i*ny*nz + (j+1)*nz + k]);
				reg[i*ny*nz + j*nz + k] += 1-abs(x[i*ny*nz + j*nz + k]*x[(i+1)*ny*nz + j*nz + k]+y[i*ny*nz + j*nz + k]*y[(i+1)*ny*nz + j*nz + k]+z[i*ny*nz + j*nz + k]*z[(i+1)*ny*nz + j*nz + k]);
				reg[i*ny*nz + j*nz + k] += 1-abs(x[i*ny*nz + j*nz + k]*x[i*ny*nz + j*nz + k-1]+y[i*ny*nz + j*nz + k]*y[i*ny*nz + j*nz + k-1]+z[i*ny*nz + j*nz + k]*z[i*ny*nz + j*nz + k-1]);
				reg[i*ny*nz + j*nz + k] += 1-abs(x[i*ny*nz + j*nz + k]*x[i*ny*nz + (j-1)*nz + k]+y[i*ny*nz + j*nz + k]*y[i*ny*nz + (j-1)*nz + k]+z[i*ny*nz + j*nz + k]*z[i*ny*nz + (j-1)*nz + k]);
				reg[i*ny*nz + j*nz + k] += 1-abs(x[i*ny*nz + j*nz + k]*x[(i-1)*ny*nz + j*nz + k]+y[i*ny*nz + j*nz + k]*y[(i-1)*ny*nz + j*nz + k]+z[i*ny*nz + j*nz + k]*z[(i-1)*ny*nz + j*nz + k]);
				
			}
		}
	}
	if (grad==1){
		for (i = 1; i < nx-1; i++)
		{
			for (j = 1; j < ny-1; j++)
			{
				for (k = 1; k < nz-1; k++)
				{
					drdz[i*ny*nz + j*nz + k] = reg[i*ny*nz + j*nz + k+1]-reg[i*ny*nz + j*nz + k];
					drdy[i*ny*nz + j*nz + k] = reg[i*ny*nz + (j+1)*nz + k]-reg[i*ny*nz + j*nz + k];
					drdx[i*ny*nz + j*nz + k] = reg[(i+1)*ny*nz + j*nz + k]-reg[i*ny*nz + j*nz + k];
					drdz[i*ny*nz + j*nz + k] += reg[i*ny*nz + j*nz + k-1]-reg[i*ny*nz + j*nz + k];
					drdy[i*ny*nz + j*nz + k] += reg[i*ny*nz + (j-1)*nz + k]-reg[i*ny*nz + j*nz + k];
					drdx[i*ny*nz + j*nz + k] += reg[(i-1)*ny*nz + j*nz + k]-reg[i*ny*nz + j*nz + k];
					drdx[i*ny*nz + j*nz + k] *=0.5;
					drdy[i*ny*nz + j*nz + k] *=0.5;
					drdz[i*ny*nz + j*nz + k] *=0.5;
				}
			}
		}


		vdAtan2(ntot,drdy,drdx,grad_phi_reg);
		vdSqr(ntot,drdx,drdx);
		vdSqr(ntot,drdy,drdy);
		vdAdd(ntot,drdy,drdx,drdy);
		vdSqrt(ntot,drdy,drdy);
		vdAtan2(ntot,drdy,drdz,grad_theta_reg);


	}
	regout[0] = cblas_dasum(ntot,reg,1);
	free(sin_th);
	free(cos_th);
	free(sin_ph);
	free(cos_ph);
	free(x);
	free(y);
	free(drdx);
	free(drdy);
	free(drdz);
	free(reg);
}

void scalar_regularization(const double * restrict a, double * restrict reg,double * restrict regout,int nx, int ny, int nz,int nq){
	int i,j,k;
	int ntot = nx*ny*nz*nq;
	for (i = 1; i < nx-1; i++)
	{
		for (j = 1; j < ny-1; j++)
		{
			for (k = 1; k < nz-1; k++)
			{	
				for (int l = 0; l < nq; ++l)
				{
					reg[(i*ny*nz + j*nz + k)*nq+l] += a[(i*ny*nz + j*nz + k)*nq+l]-a[(i*ny*nz + j*nz + k+1)*nq+l];
					reg[(i*ny*nz + j*nz + k)*nq+l] += a[(i*ny*nz + j*nz + k)*nq+l]-a[(i*ny*nz + (j+1)*nz + k)*nq+l];
					reg[(i*ny*nz + j*nz + k)*nq+l] += a[(i*ny*nz + j*nz + k)*nq+l]-a[((i+1)*ny*nz + j*nz + k)*nq+l];	
					reg[(i*ny*nz + j*nz + k)*nq+l] += a[(i*ny*nz + j*nz + k)*nq+l]-a[(i*ny*nz + j*nz + k-1)*nq+l];
					reg[(i*ny*nz + j*nz + k)*nq+l] += a[(i*ny*nz + j*nz + k)*nq+l]-a[(i*ny*nz + (j-1)*nz + k)*nq+l];
					reg[(i*ny*nz + j*nz + k)*nq+l] += a[(i*ny*nz + j*nz + k)*nq+l]-a[((i-1)*ny*nz + j*nz + k)*nq+l];		
				}
			}
		}
	}	

	regout[0] = cblas_dasum(ntot,reg,1);
}