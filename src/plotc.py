import numpy as np
from numpy import pi
import param_struct as ps
from scipy.interpolate import RegularGridInterpolator
from scipy.signal import convolve2d,decimate
from scipy.spatial.transform import Rotation as Ro
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from ctypes import *
import time
import ctypes


# npthing = np.loadtxt("coutput.txt",dtype=float)
# npthing/=(np.linalg.norm(npthing))
# npthing2 = np.loadtxt("coutput2.txt",dtype=float)
# npthing2/=(np.linalg.norm(npthing2))
# npthing3 = np.loadtxt("coutput3.txt",dtype=float)
# npthing3/=(np.linalg.norm(npthing3))
# npthing4 = np.loadtxt("coutput4.txt",dtype=float)
# npthing4/=(np.linalg.norm(npthing4))
# npthing5 = np.loadtxt("coutput5.txt",dtype=float)
# npthing5/=(np.linalg.norm(npthing5))
synthproj = np.loadtxt("synthproj.txt",dtype=float)
grad = np.loadtxt("gradient",dtype=float)
grad/=np.linalg.norm(grad)
gradph = np.loadtxt("gradient_ph",dtype=float)
gradph/=np.linalg.norm(gradph)
proj = np.loadtxt("proj.txt",dtype=float)
th = np.loadtxt("theta.txt",dtype=float)
ph = np.loadtxt("phi.txt",dtype=float)

# fig=plt.figure()
# plt.imshow(npthing.reshape((33,46)),cmap='jet')
# plt.colorbar()
# fig=plt.figure()
# plt.imshow(npthing2.reshape((33,46)),cmap='jet')
# plt.colorbar()
# fig=plt.figure()
# plt.imshow(npthing3.reshape((33,46)),cmap='jet')
# plt.colorbar()
# fig=plt.figure()
# plt.imshow(npthing4.reshape((33,46)),cmap='jet')
# plt.colorbar()
# fig=plt.figure()
# plt.imshow(npthing5.reshape((33,46)),cmap='jet')

fig=plt.figure()
plt.imshow(grad.reshape((46,33)),cmap='jet')
plt.colorbar()
fig=plt.figure()
plt.imshow(gradph.reshape((46,33)),cmap='jet')
plt.colorbar()
fig=plt.figure()
plt.imshow(np.sqrt(proj.reshape((46,29)))/np.max(np.sqrt(proj.reshape((46,29)))),cmap='jet',vmin=0)
plt.colorbar()
fig=plt.figure()
plt.imshow(np.sqrt(synthproj.reshape((46,29)))/np.max(np.sqrt(proj.reshape((46,29)))),cmap='jet')
plt.colorbar()
fig=plt.figure()
plt.imshow((np.arctan(np.sin(th.reshape((46,33)))/np.cos(th.reshape((46,33))))),cmap='hsv')
plt.colorbar()
fig=plt.figure()
plt.imshow(np.arctan(np.sin(ph.reshape((46,33)))/np.cos(ph.reshape((46,33)))),cmap='hsv')
plt.colorbar()
# fig=plt.figure()
# plt.imshow(np.cos(ph.reshape((46,33)))*np.sin(th.reshape((46,33))),cmap='hsv')
# plt.colorbar()
plt.show();