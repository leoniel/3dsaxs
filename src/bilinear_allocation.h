
    #ifndef _bilinear_allocation_h
    #define _bilinear_allocation_h


    void allocate(double * restrict, int * , const double * restrict , int * , const double * restrict , const double * restrict , const double * restrict , const double * restrict );

	void upsample(double * , const double * restrict , const int * , const int * , const int * );

	void allocate_back(double * restrict, int * ,  int *,const double * restrict, int *,  const double * restrict , const double * restrict,  const double * restrict , const double * restrict);

	void convolve_downsample_2d(double *, double *, double *, int *, int);

	void convolve_3d(double *, double *, double *, int *, int);
 
    #endif