#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <math.h>
#include "error_metric_kernel.h"
#define MKL_Complex16 complex_struct
#include "bilinear_allocation.h"
#include "angle_regularization.h"
#include "arb_projection.h"
#include "error_metric_kernel.h"
#include "mkl.h"
#include "mkl_vml.h"
#include "mkl_types.h"


#define pi 3.14159265358979323846
//void plmindex_(int*, int*, int*);
void plmon_(double *, int *, double *,int*,int*,int*);
void plmon_d1_(double *,double *, int *, double *,int*,int*,int*);

/**
	\def error_metric_kernel.c
	\brief Error metric kernel for tensor tomography.

	This is the kernel of the SAXS tensor tomography code,
	which performs the numerical differentiations and projections
	necessary for tomographic reconstruction. It is optimized for use with the
	conjugate gradient method.
*/


double pow2(double a){return a*a;} //utility function
int plmind(int k, int m){return k*(k+1)/2+m;}

/**
	\fn void _3dsaxs_error_metric_kernel(opt_inputs * restrict opt_in, kernel_inputs *restrict  kern_in, p_struct * restrict p,  s_struct * restrict s, kernel_outputs * restrict kern_out)
	\brief Main function of the error metric kernel.
	
	This is the main function of the tensor tomography error metric. It takes a series of struct pointers as parameters.
*/

void _3dsaxs_error_metric_kernel(opt_inputs * restrict opt_in, kernel_inputs *restrict  kern_in, p_struct * restrict p,  s_struct * restrict s, kernel_outputs * restrict kern_out){

	#ifdef M_0_ONLY
		printf("\nSTART, M = 0 MODE\n");
	#else
		printf("\nSTART, M = [0,1..L-1,L] MODE\n");
	#endif
	double *grad_a,*grad_theta,*grad_phi;
	grad_a = (double*) calloc(p->dims_in->num_voxels*p->num_opt_coeff,sizeof(double));
	grad_theta = (double*) calloc(p->dims_in->num_voxels,sizeof(double));
	grad_phi = (double*) calloc(p->dims_in->num_voxels,sizeof(double));
	double E=0,Ereg=0;
	FILE *fptr;
	#pragma omp parallel for ordered reduction(+: E) reduction(+: grad_a[:p->dims_in->num_voxels*p->num_opt_coeff]) reduction(+: grad_theta[:p->dims_in->num_voxels]) reduction(+:grad_phi[:p->dims_in->num_voxels])
	for (int i=0; i<p->dims_in->num_projections; i++){
		CBLAS_LAYOUT Layout;
 	    CBLAS_TRANSPOSE transa;
 	    CBLAS_TRANSPOSE transb;
 	    Layout = CblasRowMajor;
 	    transa = CblasTrans;
 	    transb = CblasTrans;
 	    int j;
 	    double R[9];		
 	    for (j = 0; j < 9; j++){
			R[j] = p->rot_exp[i*9+j];
		}
 	    double * unit_q_object;
 	    unit_q_object = (double *)malloc(sizeof(double)*3*p->dims_in->num_segments);
 	    double * cos_theta_sh_cut;
 	    cos_theta_sh_cut = (double *) malloc(sizeof(double)*p->dims_in->num_segments*p->dims_in->num_voxels);
 	     double * x_sh_cut;
 	    x_sh_cut = (double *) malloc(sizeof(double)*p->dims_in->num_segments*p->dims_in->num_voxels);
 	     double * y_sh_cut;
 	    y_sh_cut = (double *) malloc(sizeof(double)*p->dims_in->num_segments*p->dims_in->num_voxels);
 	    double * q_pp; 	 	     
 	    q_pp = (double *) malloc(sizeof(double)*3*p->dims_in->num_segments*p->dims_in->num_voxels); 	    
		double y_lm[200];
 	    for (j = 0; j < 200; j++)
 	    {
 	    	y_lm[j] = 0;
 	    }
 	    double * y_lm_reduced;
 	    y_lm_reduced = (double *) malloc(sizeof(double)*p->num_tot_coeff*p->dims_in->num_segments*
 	    				p->dims_in->num_voxels); 	   
 	    MKL_Complex16 * y_alm_sum;
 	    y_alm_sum = (MKL_Complex16 *) calloc(p->dims_in->num_segments*p->dims_in->num_voxels,
 	    				sizeof(MKL_Complex16));
 	    double * xout;
 	    xout = (double *) malloc(sizeof(double)*p->dims_in->data_shape[i*3+0]);
  	    double * yout;
 	    yout = (double *) malloc(sizeof(double)*p->dims_in->data_shape[i*3+1]);
 	    for(j=0;j<p->dims_in->data_shape[i*3+0];j++){
 	    	xout[j]=j+p->dims_in->dx[i]-ceil(p->dims_in->data_shape[i*3+0]*0.5);
 	    } 
 	    for(j=0;j<p->dims_in->data_shape[i*3+1];j++){
 	    	yout[j]=j+p->dims_in->dy[i]-ceil(p->dims_in->data_shape[i*3+1]*0.5);
 	    }
		int l,k;
 	    int l_max=s->l_max;
 	    int fortran_exit_status;
 	    cblas_dgemm(Layout,transa,transb,3,p->dims_in->num_segments,3,1,R,3,kern_in->unit_q_beamline,3,0,
 	    	unit_q_object,p->dims_in->num_segments);
		
			

		transa = CblasNoTrans;
 	    transb = CblasNoTrans;
 	    for(j=0;j<p->dims_in->num_voxels;j++){
 	    	cblas_dgemm(Layout,transa,transb,3,p->dims_in->num_segments,3,1,(kern_in->rot_str+9*j),3,
 	    				unit_q_object,p->dims_in->num_segments,0,(q_pp+j*3*p->dims_in->num_segments),
 	    				p->dims_in->num_segments);			
		} 	
		double * pl;
		for (j = 0; j < p->dims_in->num_segments; j++)
		{
		cblas_dcopy(p->dims_in->num_voxels,(q_pp+j+2*p->dims_in->num_segments),p->dims_in->num_segments*3
			,cos_theta_sh_cut+j,p->dims_in->num_segments);	

		cblas_dcopy(p->dims_in->num_voxels,(q_pp+j+1*p->dims_in->num_segments),p->dims_in->num_segments*3
			,y_sh_cut+j,p->dims_in->num_segments);

		cblas_dcopy(p->dims_in->num_voxels,(q_pp+j+0*p->dims_in->num_segments),p->dims_in->num_segments*3
			,x_sh_cut+j,p->dims_in->num_segments);	
		

		}					
		double * phi_sh_cut;
		phi_sh_cut = (double *) calloc(p->dims_in->num_segments*p->dims_in->num_voxels,sizeof(double));


		vdAtan2(p->dims_in->num_segments*p->dims_in->num_voxels,y_sh_cut,x_sh_cut,phi_sh_cut);
		
		int farg1,farg2,indin,null,kz;
		farg1=1;
		farg2=1;
		null=0;
		indin=0;
		//Macro for only M = 0		
		#ifdef M_0_ONLY
				for (j=0;j<p->dims_in->num_voxels;j++){
					for (l=0;l<p->dims_in->num_segments;l++){
						pl = cos_theta_sh_cut+j*p->dims_in->num_segments+l;
						plmon_(y_lm,&l_max,pl,&farg1,&farg2,&fortran_exit_status);
						for(k =0;k<p->num_tot_coeff;k++){
							kz=k*2;
							indin = plmind(kz,null);
								y_lm_reduced[j*(p->num_tot_coeff)*p->dims_in->num_segments+
									l*(p->num_tot_coeff)+k] = y_lm[indin];	
								}
							}
						}
				
				MKL_Complex16 * phi_factor;
				phi_factor = malloc(sizeof(MKL_Complex16)*(p->num_tot_coeff)*p->dims_in->num_segments*
							p->dims_in->num_voxels);

				for (j=0;j<p->dims_in->num_voxels;j++){
					for (l=0;l<p->dims_in->num_segments;l++){
						for(k =0;k<p->num_tot_coeff;k++){
							phi_factor[j*(p->num_tot_coeff)*p->dims_in->num_segments+l*
								(p->num_tot_coeff)+k].real =1;

							phi_factor[j*(p->num_tot_coeff)*p->dims_in->num_segments+l*
								(p->num_tot_coeff)+k].imag = 0;
								
						}
					}			
				}
				for(j=0;j<p->dims_in->num_voxels;j++){
					for(k=0;k<p->dims_in->num_segments;k++){
						for (l = 0; l <p->num_opt_coeff; ++l)
						{	
							y_alm_sum[j*p->dims_in->num_segments+k].real += 
								y_lm_reduced[j*p->num_tot_coeff*p->dims_in->num_segments+k*
								p->num_tot_coeff+l]*kern_in->a[l+j*p->num_tot_coeff]*
							phi_factor[j*p->num_tot_coeff*p->dims_in->num_segments+k*
								p->num_tot_coeff+l].real;	

								y_alm_sum[j*p->dims_in->num_segments+k].imag += 
							y_lm_reduced[j*p->num_tot_coeff*p->dims_in->num_segments+k*
								p->num_tot_coeff+l]*kern_in->a[l+j*p->num_tot_coeff]*
								phi_factor[j*p->num_tot_coeff*p->dims_in->num_segments+k*
								p->num_tot_coeff+l].imag;		
						}
					}
				}
		
				
		#else
				for (j=0;j<p->dims_in->num_voxels;j++){
					for (l=0;l<p->dims_in->num_segments;l++){
						pl = cos_theta_sh_cut+j*p->dims_in->num_segments+l;
						plmon_(y_lm,&l_max,pl,&farg1,&farg2,&fortran_exit_status);
						for(k =0;k<=s->l_max;k++){
							for(int m = 0;m <= k;m++){
								indin = plmind(k,m);
										y_lm_reduced[j*(p->num_tot_coeff)*p->dims_in->num_segments+l*
														(p->num_tot_coeff)+indin] = y_lm[indin];	
							}
						}
					}
				}	
				double * pfb = malloc(sizeof(double)*(p->num_tot_coeff)*p->dims_in->num_segments*
							p->dims_in->num_voxels);
				MKL_Complex16 * phi_factor;
				phi_factor = malloc(sizeof(MKL_Complex16)*(p->num_tot_coeff)*p->dims_in->num_segments*
							p->dims_in->num_voxels);
				for (j=0;j<p->dims_in->num_voxels;j++){
					for (l=0;l<p->dims_in->num_segments;l++){
						for(k =0;k<=s->l_max;k++){
							for(int m = 0;m <= k;m++){

									pfb[j*(p->num_tot_coeff)*p->dims_in->num_segments+
									l*(p->num_tot_coeff)+k*(k+1)/2+m] = m*phi_sh_cut[j*p->dims_in->num_segments+l];								
								
							}			
						}
					}
				}	

				vzCIS(p->num_tot_coeff*p->dims_in->num_segments*
							p->dims_in->num_voxels,pfb,phi_factor);
				free(pfb);
				for(j=0;j<p->dims_in->num_voxels;j++){
					for(k=0;k<p->dims_in->num_segments;k++){
						for (l = 0; l <= l_max; ++l)
						{	
							for (int m = 0; m <= l; ++m)
							{

									y_alm_sum[j*p->dims_in->num_segments+k].real += 
										y_lm_reduced[j*p->num_tot_coeff*p->dims_in->num_segments+
										k*p->num_tot_coeff+l*(l+1)/2 +m]*kern_in->a[l*(l+1)/2  +m+ 
										j*p->num_tot_coeff]*phi_factor[j*p->num_tot_coeff*
										p->dims_in->num_segments+k*p->num_tot_coeff+l*(l+1)/2 +m].real;

									y_alm_sum[j*p->dims_in->num_segments+k].imag += 
										y_lm_reduced[j*p->num_tot_coeff*p->dims_in->num_segments+
										k*p->num_tot_coeff+l*(l+1)/2  +m]*kern_in->a[l*(l+1)/2 +m+ 
										j*p->num_tot_coeff]*phi_factor[j*p->num_tot_coeff*
										p->dims_in->num_segments+k*p->num_tot_coeff+l*(l+1)/2 +m].imag;		
								
							}
						}
					}
				}

		#endif

  		double * data_synt;
 	    data_synt = (double *) calloc(p->dims_in->num_segments*p->dims_in->num_voxels,sizeof(double));

		for(j=0;j<p->dims_in->num_voxels*p->dims_in->num_segments;j++){
			data_synt[j] = y_alm_sum[j].real*y_alm_sum[j].real+y_alm_sum[j].imag*y_alm_sum[j].imag;

		}
		vzConj(p->dims_in->num_voxels*p->dims_in->num_segments,y_alm_sum,y_alm_sum);
		double * proj_out;
		proj_out = (double*) calloc(p->dims_in->projection_size[i]*p->dims_in->num_segments,sizeof(double));
		
		arb_projection(data_synt,p->dims_in->X,p->dims_in->Y,p->dims_in->Z,
						R,xout,yout,proj_out,p->dims_in->num_voxels,p->dims_in->num_segments,
						p->dims_in->tomo_size,(p->dims_in->data_shape+3*i));
		double * aux_diff_poisson;
		aux_diff_poisson = (double*) malloc(sizeof(double)*p->dims_in->projection_size[i]*
							p->dims_in->num_segments);

		for (j = 0; j < p->dims_in->projection_size[i]; j++){
			for (k = 0; k < p->dims_in->num_segments; k++)
			{
				aux_diff_poisson[j*p->dims_in->num_segments+k] = (sqrt(proj_out[j*p->dims_in->num_segments+k])
								-sqrt(p->data[p->dims_in->cumsize[i]*p->dims_in->num_segments+
						j*p->dims_in->num_segments+k]))*s->window_mask[p->dims_in->cumsize[i]+j];
			}	
		}
		double error_norm=0;
		for (j = 0; j < p->dims_in->projection_size[i]*p->dims_in->num_segments; j++)
		{
			error_norm += pow2(aux_diff_poisson[j]);
		}
		error_norm *= (double) 2.;
		error_norm /= (1.0*p->dims_in->num_projections*p->dims_in->nx*p->dims_in->ny);
		E += error_norm;
		if (p->settings[0]==1){
			for (j = 0; j < p->dims_in->projection_size[i]*p->dims_in->num_segments; j++)
			{	
				kern_out->synth_proj[p->dims_in->cumsize[i]*p->dims_in->num_segments+j] = proj_out[j];
			}
		}
		if (p->settings[1]==1){	
			double * aux_grad_poisson,*aux_grad_poisson_vol;
			MKL_Complex16 *ymn_aux_vol;
			aux_grad_poisson = (double*) calloc(p->dims_in->projection_size[i]*p->dims_in->num_segments,sizeof(double));
			aux_grad_poisson_vol = (double*) calloc(p->dims_in->num_voxels*p->dims_in->num_segments,sizeof(double));
			ymn_aux_vol = (MKL_Complex16*) calloc(p->dims_in->num_voxels*p->dims_in->num_segments*p->num_tot_coeff,sizeof(MKL_Complex16));
			
			for (j = 0; j < p->dims_in->projection_size[i]*p->dims_in->num_segments; j++)
			{
				aux_grad_poisson[j] = aux_diff_poisson[j] / ((sqrt(proj_out[j])+1.e-10)*1.0*
								p->dims_in->num_projections*p->dims_in->nx*p->dims_in->ny);
			}
			arb_back_projection(aux_grad_poisson_vol,aux_grad_poisson,(p->dims_in->data_shape+3*i),p->dims_in->X,p->dims_in->Y,p->dims_in->Z,R,xout,yout,p->dims_in->num_voxels,p->dims_in->tomo_size,p->dims_in->num_segments);
			double * grad_a_aux;
			double ymnauxsum=0;
			grad_a_aux = (double*) calloc(p->num_opt_coeff*p->dims_in->num_voxels,sizeof(double));
			if (p->settings[2]==1){
				MKL_Complex16 * Ylm_complex;
				Ylm_complex = (MKL_Complex16*) calloc(p->dims_in->num_voxels*
											p->dims_in->num_segments*
											p->num_tot_coeff,sizeof(MKL_Complex16));

				for (j = 0; j < p->dims_in->num_voxels*p->dims_in->num_segments; j++)
				{
					for (k = 0; k < p->num_tot_coeff; k++)
					{
						Ylm_complex[j*p->num_tot_coeff+k].real=y_lm_reduced[j*p->num_tot_coeff+k]*
											phi_factor[j*p->num_tot_coeff+k].real;
						Ylm_complex[j*p->num_tot_coeff+k].imag=y_lm_reduced[j*p->num_tot_coeff+k]*
											phi_factor[j*p->num_tot_coeff+k].imag;
					}
				}
				for (j = 0; j < p->dims_in->num_voxels*p->dims_in->num_segments; j++)
				{
					for (k = 0; k < p->num_tot_coeff; k++)
					{

						ymn_aux_vol[j*p->num_tot_coeff+k].real = Ylm_complex[j*p->num_tot_coeff+k].real*y_alm_sum[j].real - Ylm_complex[j*p->num_tot_coeff+k].imag*y_alm_sum[j].imag;
						ymn_aux_vol[j*p->num_tot_coeff+k].imag = Ylm_complex[j*p->num_tot_coeff+k].real*y_alm_sum[j].imag + Ylm_complex[j*p->num_tot_coeff+k].imag*y_alm_sum[j].real;

					}
				}
				free(Ylm_complex);
				#ifdef M_0_ONLY
					for (j = 0; j < p->dims_in->num_voxels; j++)
					{
						for (l = 0; l < p->dims_in->num_segments; l++)
						{
							for (int L = 0; L < p->num_tot_coeff; ++L)
							{	

								grad_a_aux[j*p->num_opt_coeff+L] += 
									4*aux_grad_poisson_vol[j*p->dims_in->num_segments+l]*
									(ymn_aux_vol[j*p->num_tot_coeff*p->dims_in->num_segments+ l*p->num_tot_coeff+L*(L+1)/2+m].real);
							}
						}
					}
				#else
					for (j = 0; j < p->dims_in->num_voxels; j++)
					{
						for (l = 0; l < p->dims_in->num_segments; l++)
						{
							for (int L = 0; L <= l_max; ++L)
							{	
								for (int m = 0; m <= L; ++m)
								{
									
										grad_a_aux[j*p->num_opt_coeff+L*(L+1)/2+m] += 
											4*aux_grad_poisson_vol[j*p->dims_in->num_segments+l]*
											(ymn_aux_vol[j*p->num_tot_coeff*p->dims_in->num_segments+ l*p->num_tot_coeff+L*(L+1)/2+m].real);
									
								}		
							}
						}
					}
				#endif
					for (j = 0; j < p->dims_in->num_voxels*p->num_opt_coeff; j++)
					{
						grad_a[j] += grad_a_aux[j];
					}
			}

			free(ymn_aux_vol);
			free(grad_a_aux);
			free(aux_grad_poisson);
			if (p->settings[3]==1){
				 //Macro for only M = 0
				#ifdef M_0_ONLY
					transa = CblasNoTrans;
	 			    transb = CblasNoTrans;
	 	   			double * dq_pp_dth;
	 	    		dq_pp_dth = (double *) malloc(sizeof(double)*3*p->dims_in->num_segments*
	 	    							p->dims_in->num_voxels);
					for(j=0;j<p->dims_in->num_voxels;j++){
	 	    			cblas_dgemm(Layout,transa,transb,3,p->dims_in->num_segments,3,1,
	 	    				(kern_in->rot_str_diff_theta+9*j),3,unit_q_object,
	 	    				p->dims_in->num_segments,0,(dq_pp_dth+j*3*p->dims_in->num_segments),
	 	    				p->dims_in->num_segments);		
					}

					double * dq_pp_dph;
	 	    		dq_pp_dph = (double *) malloc(sizeof(double)*3*p->dims_in->num_segments*
	 	    							p->dims_in->num_voxels);

					for(j=0;j<p->dims_in->num_voxels;j++){
	 	    			cblas_dgemm(Layout,transa,transb,3,p->dims_in->num_segments,
	 	    				3,1,(kern_in->rot_str_diff_phi+9*j),3,unit_q_object,
	 	    				p->dims_in->num_segments,0,(dq_pp_dph+j*3*p->dims_in->num_segments),
	 	    				p->dims_in->num_segments);			
					}

					
	 			   
					double * sin_theta_sh_cut;
					sin_theta_sh_cut = (double *) malloc(sizeof(double)*p->dims_in->num_segments*
												p->dims_in->num_voxels);

					double * aux1, *aux2;
					aux1 = (double *) malloc(sizeof(double)*p->dims_in->num_segments*p->dims_in->num_voxels);
					aux2 = (double *) malloc(sizeof(double)*p->dims_in->num_segments*p->dims_in->num_voxels);

					vdPowx(p->dims_in->num_segments*p->dims_in->num_voxels,cos_theta_sh_cut,2.,aux1);

					for (j = 0; j < p->dims_in->num_segments*p->dims_in->num_voxels; j++)
					{
						aux2[j] = 1. - aux1[j];
					}

					vdSqrt(p->dims_in->num_voxels*p->dims_in->num_segments,aux2,sin_theta_sh_cut);
					free(aux1);
					free(aux2);
	 			    double * y_lm_reduced_ext;
	 	 		    y_lm_reduced_ext = (double *) malloc(sizeof(double)*(p->num_opt_coeff)*
	 	 		    	p->dims_in->num_segments*p->dims_in->num_voxels);

	 	   			double * y_alm_sum_ext;
	 	   			y_alm_sum_ext = (double *) malloc(sizeof(double)*p->dims_in->num_segments*
	 	   				p->dims_in->num_voxels);

					double * y_lm_ext;
					y_lm_ext = (double*) malloc(sizeof(double)*(1+p->num_opt_coeff*p->num_opt_coeff)*2);
					for (j=0;j<p->dims_in->num_voxels;j++){
						for (l=0;l<p->dims_in->num_segments;l++){
							plmon_d1_(y_lm,y_lm_ext,&l_max,(cos_theta_sh_cut+j*p->dims_in->num_segments+l)
								,&farg1,&farg2,&fortran_exit_status);

							for(k =0;k<p->num_opt_coeff;k++){
								kz = 2*k;
								indin = plmind(kz,null);

								y_lm_reduced_ext[j*(p->num_opt_coeff)*p->dims_in->num_segments+l*
									(p->num_opt_coeff)+k] = y_lm_ext[indin];
												
							}
						}
					}
					for(j=0;j<p->dims_in->num_voxels;j++){
						for (k = 0; k< p->dims_in->num_segments; k++){
							y_alm_sum_ext[j*p->dims_in->num_segments +k] = cblas_ddot(p->num_opt_coeff,
								(y_lm_reduced_ext+k*(p->num_opt_coeff)+j*p->num_opt_coeff*
									p->dims_in->num_segments),1,(kern_in->a+j*(p->num_opt_coeff)),1);
						}
					}

					vdDiv(p->dims_in->num_voxels*p->dims_in->num_segments,y_alm_sum_ext,
													sin_theta_sh_cut,y_alm_sum_ext);				
					
					for (j = 0; j < p->dims_in->num_voxels*p->dims_in->num_segments; j++)
					{
						y_alm_sum_ext[j] *= y_alm_sum[j].real;
					}
				
					free(sin_theta_sh_cut);

					double * grad_theta_struct_aux_vol;
					grad_theta_struct_aux_vol = (double*) calloc(p->dims_in->num_segments*
												p->dims_in->num_voxels,sizeof(double));
					double * grad_phi_struct_aux_vol;
					grad_phi_struct_aux_vol = (double*) calloc(p->dims_in->num_segments*
												p->dims_in->num_voxels,sizeof(double));

					for (j = 0; j < p->dims_in->num_voxels; j++)
					{				
						for (k = 0; k < p->dims_in->num_segments; k++)
						{
							grad_theta_struct_aux_vol[j*p->dims_in->num_segments + k] = 
									y_alm_sum_ext[j*p->dims_in->num_segments + k]*
									dq_pp_dth[2*p->dims_in->num_segments+ j*3*p->dims_in->num_segments+k];

							grad_phi_struct_aux_vol[j*p->dims_in->num_segments + k] = 
									y_alm_sum_ext[j*p->dims_in->num_segments + k]*
									dq_pp_dph[2*p->dims_in->num_segments+ j*3*p->dims_in->num_segments+k];
						}
					}							
					free(y_alm_sum_ext);			
					free(dq_pp_dth);
					free(dq_pp_dph);
					double * grad_theta_struct_red;
					grad_theta_struct_red = (double*) calloc(p->dims_in->num_voxels,sizeof(double));
					double * grad_phi_struct_red;
					grad_phi_struct_red = (double*) calloc(p->dims_in->num_voxels,sizeof(double));	
					for (j = 0; j < p->dims_in->num_voxels; j++)
					{
						for (l = 0; l < p->dims_in->num_segments; l++)
						{					
							grad_theta_struct_red[j] += 4*aux_grad_poisson_vol[j*p->dims_in->num_segments+l]*
													grad_theta_struct_aux_vol[j*p->dims_in->num_segments+l];
							grad_phi_struct_red[j] += 4*aux_grad_poisson_vol[j*p->dims_in->num_segments+l]*
													grad_phi_struct_aux_vol[j*p->dims_in->num_segments+l];
						}
					}
					for (j = 0; j < p->dims_in->num_voxels; j++)
					{
						grad_phi[j] += grad_phi_struct_red[j];
						grad_theta[j] += grad_theta_struct_red[j];
					}
					free(y_lm_ext);		
					free(y_lm_reduced_ext);
					free(grad_theta_struct_aux_vol);
					free(grad_phi_struct_aux_vol);
					free(grad_theta_struct_red);
					free(grad_phi_struct_red);
				#else
					transa = CblasNoTrans;
	 			    transb = CblasNoTrans;
	 	   			double * dq_pp_dth;
	 	    		dq_pp_dth = (double *) malloc(sizeof(double)*3*p->dims_in->num_segments*
	 	    								p->dims_in->num_voxels);
					for(j=0;j<p->dims_in->num_voxels;j++){
	 	    			cblas_dgemm(Layout,transa,transb,3,p->dims_in->num_segments,3,1,
	 	    				(kern_in->rot_str_diff_theta+9*j),3,unit_q_object,p->dims_in->num_segments,0,(
	 	    				dq_pp_dth+j*3*p->dims_in->num_segments),p->dims_in->num_segments);		
			
					}
					double * dq_pp_dph;
	 	    		dq_pp_dph = (double *) malloc(sizeof(double)*3*p->dims_in->num_segments*p->dims_in->num_voxels);
					for(j=0;j<p->dims_in->num_voxels;j++){
	 	    			cblas_dgemm(Layout,transa,transb,3,p->dims_in->num_segments,3,1,
	 	    				(kern_in->rot_str_diff_phi+9*j),3,unit_q_object,p->dims_in->num_segments,0,
	 	    				(dq_pp_dph+j*3*p->dims_in->num_segments),p->dims_in->num_segments);			
					}

					
	 			   
					double * sin_theta_sh_cut;
					sin_theta_sh_cut = (double *) malloc(sizeof(double)*p->dims_in->num_segments*
										p->dims_in->num_voxels);

					double * aux1, *aux2;
					aux1 = (double *) malloc(sizeof(double)*p->dims_in->num_segments*
										p->dims_in->num_voxels);
					aux2 = (double *) malloc(sizeof(double)*p->dims_in->num_segments*
										p->dims_in->num_voxels);

					vdPowx(p->dims_in->num_segments*p->dims_in->num_voxels,cos_theta_sh_cut,2.,aux1);

					for (j = 0; j < p->dims_in->num_segments*p->dims_in->num_voxels; j++)
					{
						aux2[j] = 1. - aux1[j];
					}

					vdSqrt(p->dims_in->num_voxels*p->dims_in->num_segments,aux2,sin_theta_sh_cut);

				
					free(aux2);
					MKL_Complex16 *aux_complex, *aux_complex2,*ph_in,*ph_out, *const_val;
					aux_complex = calloc(p->dims_in->num_voxels*p->dims_in->num_segments,sizeof(MKL_Complex16));
					aux_complex2 = calloc(p->dims_in->num_voxels*p->dims_in->num_segments,sizeof(MKL_Complex16));
					ph_in = calloc(p->dims_in->num_voxels*p->dims_in->num_segments,sizeof(MKL_Complex16));
					ph_out = calloc(p->dims_in->num_voxels*p->dims_in->num_segments,sizeof(MKL_Complex16));
					const_val = calloc(p->dims_in->num_voxels*p->dims_in->num_segments,sizeof(MKL_Complex16));

					// for (j = 0; j < p->dims_in->num_segments*p->dims_in->num_voxels; j++)
					// {
					// 	ph_in[j].real = (double) 0.;
					// 	ph_in[j].imag = (double) phi_sh_cut[j];
					// 	aux_complex[j].real = (double) aux1[j];
					// 	aux_complex[j].imag = (double) 0.;
					// }
					free(aux1);
	 			    MKL_Complex16 * y_lm_reduced_ext,*y_lm_reduced_dphi;
	 	 		    y_lm_reduced_ext = (MKL_Complex16 * ) malloc(sizeof(MKL_Complex16)*(p->num_tot_coeff)*
	 	 		    					p->dims_in->num_segments*p->dims_in->num_voxels);
	 	   			y_lm_reduced_dphi = (MKL_Complex16 * ) malloc(sizeof(MKL_Complex16)*(p->num_tot_coeff)*
	 	   								p->dims_in->num_segments*p->dims_in->num_voxels);
	 	   			
	 	   			MKL_Complex16* y_alm_sum_ext,*y_alm_sum_dphi;
	 	   			y_alm_sum_ext = (MKL_Complex16*) calloc(p->dims_in->num_segments*p->dims_in->num_voxels,sizeof(MKL_Complex16));
					y_alm_sum_dphi = (MKL_Complex16*) calloc(p->dims_in->num_segments*p->dims_in->num_voxels,sizeof(MKL_Complex16));

					double * y_lm_ext;
				

					y_lm_ext = (double*) calloc((1+p->num_tot_coeff*p->num_tot_coeff)*2,sizeof(double));
					for (j=0;j<p->dims_in->num_voxels;j++){
						for (l=0;l<p->dims_in->num_segments;l++){
							plmon_d1_(y_lm,y_lm_ext,&l_max,(cos_theta_sh_cut+j*p->dims_in->num_segments+l),&farg1,&farg2,&fortran_exit_status);
							for(k =0;k<=s->l_max;k++){
								for(int m = 0;m <= k;m++){
									indin = plmind(k,m);
										y_lm_reduced_ext[j*(p->num_tot_coeff)*p->dims_in->num_segments+l*
													(p->num_tot_coeff)+indin].real = y_lm_ext[indin]*phi_factor[j*(p->num_tot_coeff)*p->dims_in->num_segments+l*
													(p->num_tot_coeff)+indin].real;

										y_lm_reduced_ext[j*(p->num_tot_coeff)*p->dims_in->num_segments+l*
													(p->num_tot_coeff)+indin].imag = y_lm_ext[indin]*phi_factor[j*(p->num_tot_coeff)*p->dims_in->num_segments+l*
													(p->num_tot_coeff)+indin].imag;

										y_lm_reduced_dphi[j*(p->num_tot_coeff)*p->dims_in->num_segments+l*
													(p->num_tot_coeff)+indin].real = -m*y_lm[indin]*phi_factor[j*(p->num_tot_coeff)*p->dims_in->num_segments+l*
													(p->num_tot_coeff)+indin].imag;

										y_lm_reduced_dphi[j*(p->num_tot_coeff)*p->dims_in->num_segments+l*
													(p->num_tot_coeff)+indin].imag = m*y_lm[indin]*phi_factor[j*(p->num_tot_coeff)*p->dims_in->num_segments+l*
													(p->num_tot_coeff)+indin].real;
									
								}				
							}
						}
					}

					for(j=0;j<p->dims_in->num_voxels;j++)
					{
						for (k = 0; k< p->dims_in->num_segments; k++)
						{
							for (int w = 0; w < p->num_tot_coeff; w++)
							{
								y_alm_sum_ext[j*p->dims_in->num_segments +k].real += (y_lm_reduced_ext[k*(p->num_tot_coeff)+j*
												p->num_tot_coeff*p->dims_in->num_segments+w].real)*kern_in->a[j*(p->num_tot_coeff)+w];

								y_alm_sum_dphi[j*p->dims_in->num_segments +k].real += (y_lm_reduced_dphi[k*(p->num_tot_coeff)+j*
												p->num_tot_coeff*p->dims_in->num_segments+w].real)*kern_in->a[j*(p->num_tot_coeff)+w];

								y_alm_sum_dphi[j*p->dims_in->num_segments +k].imag += (y_lm_reduced_dphi[k*(p->num_tot_coeff)+j*
												p->num_tot_coeff*p->dims_in->num_segments+w].imag)*kern_in->a[j*(p->num_tot_coeff)+w];

								y_alm_sum_ext[j*p->dims_in->num_segments +k].imag += (y_lm_reduced_ext[k*(p->num_tot_coeff)+j*
												p->num_tot_coeff*p->dims_in->num_segments+w].imag)*kern_in->a[j*(p->num_tot_coeff)+w];

							}	
						}
					}				
					free(y_lm_ext);		
					free(y_lm_reduced_ext);
					free(y_lm_reduced_dphi);

					double * dTH_dth, *dTH_dph, *dPH_dph, *dPH_dth;
					dTH_dth = (double*) calloc(p->dims_in->num_voxels*p->dims_in->num_segments,sizeof(double)); 
					dTH_dph = (double*) calloc(p->dims_in->num_voxels*p->dims_in->num_segments,sizeof(double));
					dPH_dth = (double*) calloc(p->dims_in->num_voxels*p->dims_in->num_segments,sizeof(double));
					dPH_dph = (double*) calloc(p->dims_in->num_voxels*p->dims_in->num_segments,sizeof(double));
					vdCos(p->dims_in->num_voxels*p->dims_in->num_segments,phi_sh_cut,phi_sh_cut);
					vdSqr(p->dims_in->num_voxels*p->dims_in->num_segments,phi_sh_cut,phi_sh_cut);
					for (j = 0; j < p->dims_in->num_voxels; j++)
					{	
						for (k = 0; k < p->dims_in->num_segments; k++)
						{
						
							dPH_dth[j*p->dims_in->num_segments+k] = ((phi_sh_cut[j*p->dims_in->num_segments+k]))*(dq_pp_dth[p->dims_in->num_segments+
									j*3*p->dims_in->num_segments+k]*x_sh_cut[j*p->dims_in->num_segments+k] 
							- dq_pp_dth[j*3*p->dims_in->num_segments+k]*y_sh_cut[j*p->dims_in->num_segments+k])/(pow2(x_sh_cut[j*p->dims_in->num_segments+k]));

							dPH_dph[j*p->dims_in->num_segments+k] = ((phi_sh_cut[j*p->dims_in->num_segments+k]))*(dq_pp_dph[p->dims_in->num_segments+
									j*3*p->dims_in->num_segments+k]*x_sh_cut[j*p->dims_in->num_segments+k] 
							- dq_pp_dph[j*3*p->dims_in->num_segments+k]*y_sh_cut[j*p->dims_in->num_segments+k])/(pow2(x_sh_cut[j*p->dims_in->num_segments+k]));

							dTH_dth[j*p->dims_in->num_segments+k] = dq_pp_dth[2*p->dims_in->num_segments+ j*3*p->dims_in->num_segments+k]/
										sin_theta_sh_cut[j*p->dims_in->num_segments+k];

							dTH_dph[j*p->dims_in->num_segments+k] = dq_pp_dph[2*p->dims_in->num_segments+ j*3*p->dims_in->num_segments+k]/
										sin_theta_sh_cut[j*p->dims_in->num_segments+k];
						}
					}
			
					for (j = 0; j < p->dims_in->num_segments*p->dims_in->num_voxels; j++)
					{
						// aux_complex[j].real = (double) 
						// aux_complex[j].imag = (double) dTH_dth[j]*y_alm_sum_ext[j].imag;
						aux_complex[j].real = (double) dPH_dth[j]*y_alm_sum_dphi[j].real+dTH_dth[j]*y_alm_sum_ext[j].real;
						aux_complex[j].imag = (double) dPH_dth[j]*y_alm_sum_dphi[j].imag+dTH_dth[j]*y_alm_sum_ext[j].real;

					}
					free(sin_theta_sh_cut);


					// vzMul(p->dims_in->num_voxels*p->dims_in->num_segments,y_alm_sum_ext,aux_complex,aux_complex);
					// vzMul(p->dims_in->num_voxels*p->dims_in->num_segments,y_alm_sum_dphi,aux_complex2,aux_complex2);				
					// vzSub(p->dims_in->num_voxels*p->dims_in->num_segments,aux_complex2,aux_complex,aux_complex);				
					vzMul(p->dims_in->num_voxels*p->dims_in->num_segments,aux_complex,y_alm_sum,aux_complex);
					for (j = 0; j < p->dims_in->num_segments*p->dims_in->num_voxels; j++)
					{
						// ph_in[j].real = (double) dPH_dph[j]*y_alm_sum_dphi[j].real;
						// ph_in[j].imag = (double) dPH_dph[j]*y_alm_sum_dphi[j].imag;
						aux_complex2[j].real = (double) dPH_dph[j]*y_alm_sum_dphi[j].real+dTH_dph[j]*y_alm_sum_ext[j].real;
						aux_complex2[j].imag = (double) dPH_dph[j]*y_alm_sum_dphi[j].imag+dTH_dph[j]*y_alm_sum_ext[j].imag;

					}
					//vzMul(p->dims_in->num_voxels*p->dims_in->num_segments,y_alm_sum_ext,aux_complex2,aux_complex2);
					//vzMul(p->dims_in->num_voxels*p->dims_in->num_segments,y_alm_sum_dphi,ph_in,ph_in);				
					//vzSub(p->dims_in->num_voxels*p->dims_in->num_segments,ph_in,aux_complex2,aux_complex2);				
					vzMul(p->dims_in->num_voxels*p->dims_in->num_segments,aux_complex2,y_alm_sum,aux_complex2);
					free(y_alm_sum_ext);
					free(y_alm_sum_dphi);
					double * grad_theta_struct_aux_vol;
					grad_theta_struct_aux_vol = (double*) calloc(p->dims_in->num_segments*p->dims_in->num_voxels,sizeof(double));
					double * grad_phi_struct_aux_vol;
					grad_phi_struct_aux_vol = (double*) calloc(p->dims_in->num_segments*p->dims_in->num_voxels,sizeof(double));

					for (j = 0; j < p->dims_in->num_voxels; j++)
					{				
						for (k = 0; k < p->dims_in->num_segments; k++)
						{
							grad_theta_struct_aux_vol[j*p->dims_in->num_segments + k] = 
															(aux_complex[j*p->dims_in->num_segments + k].real); //sine part

							grad_phi_struct_aux_vol[j*p->dims_in->num_segments + k] = 
															(aux_complex2[j*p->dims_in->num_segments + k].real);	//sine part
						}
					}		
					free(aux_complex);
					free(dPH_dth);
					free(dTH_dth);
					free(dPH_dph);
					free(dTH_dph);

					free(aux_complex2);					
					free(ph_in);
					free(ph_out);
					free(dq_pp_dth);
					free(dq_pp_dph);
					double * grad_theta_struct_red;
					grad_theta_struct_red = (double*) calloc(p->dims_in->num_voxels,sizeof(double));
					double * grad_phi_struct_red;
					grad_phi_struct_red = (double*) calloc(p->dims_in->num_voxels,sizeof(double));	
					for (j = 0; j < p->dims_in->num_voxels; j++)
					{
						for (l = 0; l < p->dims_in->num_segments; l++)
						{					
							grad_theta_struct_red[j] += 4*aux_grad_poisson_vol[j*p->dims_in->num_segments+l]*
														grad_theta_struct_aux_vol[j*p->dims_in->num_segments+l];
							grad_phi_struct_red[j] += 4*aux_grad_poisson_vol[j*p->dims_in->num_segments+l]*
														grad_phi_struct_aux_vol[j*p->dims_in->num_segments+l];
						}
					}
					for (j = 0; j < p->dims_in->num_voxels; j++)
					{
						grad_phi[j] += grad_phi_struct_red[j];
						grad_theta[j] += grad_theta_struct_red[j];
					}
					free(const_val);
					free(grad_theta_struct_aux_vol);
					free(grad_phi_struct_aux_vol);
					free(grad_theta_struct_red);
					free(grad_phi_struct_red);
				#endif
				
			}
			free(aux_grad_poisson_vol);
		}
		free(aux_diff_poisson);
		if (1){
			for (j = 0; j < p->dims_in->projection_size[i]*p->dims_in->num_segments; j++)
			{
				kern_out->synth_proj[p->dims_in->cumsize[i]*p->dims_in->num_segments + j] = proj_out[j];
			}
		}
		free(phi_sh_cut);
		free(unit_q_object);
		free(q_pp);
		free(cos_theta_sh_cut);
		free(y_lm_reduced);
		free(y_alm_sum);
		free(xout);
		free(yout);
		free(proj_out);
		free(data_synt);
		free(phi_factor);
		// #ifndef M_0_ONLY
		// // free(a_extend);
		// #endif
		free(x_sh_cut);
		free(y_sh_cut);
	}

	double E_store = E;
	// if (p->settings[1])
	// {
	// 	//Error plot thing
	// }		
	double Ereg_old = 0;
	if (p->settings[4])
	{			
		double regout;
		if (p->settings[1])
		{
			double * grad_theta_reg, * grad_phi_reg;
			grad_theta_reg = (double*) malloc(sizeof(double)*p->dims_in->num_voxels);
			grad_phi_reg = (double*) malloc(sizeof(double)*p->dims_in->num_voxels);

			angle_regularization(kern_in->theta_struct,kern_in->phi_struct,grad_theta_reg,grad_phi_reg,
				&regout,p->dims_in->nx,p->dims_in->ny,p->dims_in->nz,1);

			for (int j = 0; j < p->dims_in->num_voxels; j++){
				grad_phi_reg[j] *= opt_in->regularization_angle_coeff/(double) p->dims_in->num_voxels;
				grad_theta_reg[j] *= opt_in->regularization_angle_coeff/(double) p->dims_in->num_voxels;
			}
			vdAdd(p->dims_in->num_voxels,grad_theta,grad_theta_reg,grad_theta);
			vdAdd(p->dims_in->num_voxels,grad_phi,grad_phi_reg,grad_phi);

			free(grad_theta_reg);
			free(grad_phi_reg);
		} else {
			double * grad_theta_reg, * grad_phi_reg;
			grad_theta_reg = (double*) malloc(sizeof(double)*p->dims_in->num_voxels);
			grad_phi_reg = (double*) malloc(sizeof(double)*p->dims_in->num_voxels);

			angle_regularization(kern_in->theta_struct,kern_in->phi_struct,grad_theta_reg,grad_phi_reg,
				&regout,p->dims_in->nx,p->dims_in->ny,p->dims_in->nz,0);

			free(grad_theta_reg);
			free(grad_phi_reg);
		}
		Ereg = opt_in->regularization_angle_coeff*regout/(double) p->dims_in->num_voxels;
		E=E+Ereg;
		Ereg_old=Ereg;

	//	printf("total error = data_error + regul_error\n%f = %f + %f \n", E, E_store, Ereg);
	} else {
	//	printf("data_error= %f\n", E_store);
	}
	double *aux;

	aux = (double*) malloc(sizeof(double)*p->dims_in->num_voxels*p->dims_in->num_segments);

	if (p->settings[1]){
		if (p->settings[2]&p->settings[5]){
			//int indims[4] = {p->dims_in->nx,p->dims_in->ny,p->dims_in->nz,p->num_opt_coeff};
			//convolve_3d(aux,grad_a,opt_in->_3d_kernel,indims,opt_in->_3d_kernel_size);
			//printf("Convolving 3D...\n");

		}

		if (p->settings[8]){
			double *reg;
			double regout;
			reg = (double*) calloc(p->dims_in->num_voxels*p->num_opt_coeff,sizeof(double));

			scalar_regularization(kern_in->a, reg, &regout,p->dims_in->nx,p->dims_in->ny, 
				p->dims_in->nz,p->num_opt_coeff);

			for (int j = 0; j < p->dims_in->num_voxels*p->num_opt_coeff; j++){
				reg[j] *= p->regularization_scalar_coeff/((double) p->dims_in->num_voxels*6);
			}
			vdAdd(p->dims_in->num_voxels*p->num_opt_coeff,grad_a,reg,grad_a);
			Ereg = p->regularization_scalar_coeff*regout/(double) (p->dims_in->num_voxels*6);
			E=E+Ereg;
			free(reg);

		}
		printf("total error = data_error + scalar_regul_error + angle_regul_error\n%f = %f + %f + %f\n", E, E_store, Ereg, Ereg_old);

		if (p->settings[2]){
			double * packed;
			packed = (double*) malloc(sizeof(double)*p->dims_in->num_voxels);

			for (int i = 0; i < p->num_opt_coeff; ++i)
			{
				vdPackI(p->dims_in->num_voxels,(grad_a+i),p->num_opt_coeff,packed);
				vdMul(p->dims_in->num_voxels,packed,s->mask_3d_d,packed);
				vdUnpackI(p->dims_in->num_voxels,packed,(grad_a+i),p->num_opt_coeff);

			}
			free(packed);
		}
		if (p->settings[3]){
			vdMul(p->dims_in->num_voxels,grad_theta,s->mask_3d_d,grad_theta);
			vdMul(p->dims_in->num_voxels,grad_phi,s->mask_3d_d,grad_phi);
		}
	}
	free(aux);
	double  ecoeff = 0;
	// if (p->settings[2]){
	// 	double *tracker;
	// 	tracker = (double*) calloc(p->num_opt_coeff,sizeof(double));
	// 	for(int j = 0;j<p->dims_in->num_voxels;j++){
	// 		for(int i=0;i<p->num_opt_coeff;i++){
	// 			if (kern_in->a[j*p->num_opt_coeff + i]>opt_in->coeff_soft_limits_high[i]){
	// 				tracker[i] += kern_in->a[j*p->num_opt_coeff + i]-opt_in->coeff_soft_limits_high[i];
	// 			} else if(kern_in->a[j*p->num_opt_coeff  + i]<opt_in->coeff_soft_limits_low[i]){
	// 				tracker[i] += kern_in->a[j*p->num_opt_coeff + i]-opt_in->coeff_soft_limits_low[i];
	// 			}
	// 		}
	// 	}
	// 	printf("Applying soft limits\n");
	// 	vdPowx(p->num_opt_coeff,tracker,2.,tracker);
	// 	vdMul(p->num_opt_coeff,tracker,opt_in->soft_limit_weight_coeff,tracker);
	// 	for (int i = 0; i < p->num_opt_coeff; ++i)
	// 	{	
	// 		E += tracker[i];
	// 		ecoeff+=tracker[i];
	// 	}
	// 	free(tracker);
	// }
	printf("Coefficient error: %f\n", ecoeff);
	kern_out->E_out = E;
	kern_out->E_reg = Ereg;

	//Note: The gradients are initialized at 0, so this will work and is needed
	//regardless of whether they are optimized for. Otherwise various things
	//will fail due to uninitialized values.
	if (p->settings[1])
	{
	cblas_dcopy(p->dims_in->num_voxels,grad_theta,1,kern_out->grad_theta,1);
	cblas_dcopy(p->dims_in->num_voxels,grad_phi,1,kern_out->grad_phi,1);
	cblas_dcopy(p->dims_in->num_voxels*p->num_opt_coeff,grad_a,1,kern_out->grad_a,1);
	int ind=0;
	fptr = fopen("./synthproj.txt","w");
	double buffer=0;

	for (int i = 0; i < p->dims_in->projection_size[5]; ++i)
	{		
		buffer=0;
		for (int j = 0; j < 8; j++)
		{

			buffer +=kern_out->synth_proj[p->dims_in->cumsize[5]*p->dims_in->num_segments + ind];
			ind++;

		}	
		fprintf(fptr, "%.7e ", buffer);
	}
	fclose(fptr);
	fptr = fopen("./theta.txt","w");
	int indinc=0;
	ind=(int) p->dims_in->nz/2.;
	double tau = 2*pi;
	for (int i = 0; i < p->dims_in->nx; ++i)
	{
		for (int j = 0; j < p->dims_in->ny; ++j)
		{
			indinc=ind + i*p->dims_in->ny*p->dims_in->nz+j*p->dims_in->nz;
			fprintf(fptr, "%.7e ", kern_in->theta_struct[indinc]);
		}
	}
	fclose(fptr);	
	fptr = fopen("./phi.txt","w");
	indinc=0;
	ind=(int) p->dims_in->nz/2.;
	tau = 2*pi;
	for (int i = 0; i < p->dims_in->nx; ++i)
	{
		for (int j = 0; j < p->dims_in->ny; ++j)
		{
			indinc=ind + i*p->dims_in->ny*p->dims_in->nz+j*p->dims_in->nz;
			fprintf(fptr, "%.7e ", kern_in->phi_struct[indinc]);
		}
	}
	fclose(fptr);
	ind=(int) p->dims_in->nz/2.;
	indinc=0;
	fptr = fopen("./gradient","w");
	for (int i = 0; i < p->dims_in->nx; ++i)
	{
		for (int j = 0; j < p->dims_in->ny; ++j)
		{
			indinc=ind + i*p->dims_in->ny*p->dims_in->nz+j*p->dims_in->nz;
			fprintf(fptr, "%.7e ", grad_theta[indinc]);
		}
	}
	fclose(fptr);
	ind=(int) p->dims_in->nz/2.;
	indinc=0;
	fptr = fopen("./gradient_ph","w");
	for (int i = 0; i < p->dims_in->nx; ++i)
	{
		for (int j = 0; j < p->dims_in->ny; ++j)
		{
			indinc=ind + i*p->dims_in->ny*p->dims_in->nz+j*p->dims_in->nz;
			fprintf(fptr, "%.7e ", grad_phi[indinc]);
		}
	}
	fclose(fptr);
	ind=0;
	fptr = fopen("./proj.txt","w");
	for (int i = 0; i < p->dims_in->projection_size[5]; ++i)
	{
		buffer=0;

		for (int j = 0; j < 8; j++)
		{
			buffer+=p->data[p->dims_in->cumsize[5]*p->dims_in->num_segments + ind];
			ind++;
		}
		fprintf(fptr, "%.7e ", buffer);
	}
	fclose(fptr);
	printf("Writing complete!\n");
	} else {

	printf("No gradient calculated!\n");		
	printf("total error = %f\n", E);

	}
	free(grad_theta);
	free(grad_phi);
	free(grad_a);
	return;
}

void _3dsaxs_error_metric( opt_inputs * restrict opt_in, p_struct * restrict  p,  s_struct * restrict s, outputs * restrict op){	
	kernel_inputs *kern_in;
	kern_in = malloc(sizeof(kernel_inputs));
	int num_voxels = p->dims_in->num_voxels;
	kern_in->theta_struct = (double*) malloc(sizeof(double)*num_voxels);
	kern_in->phi_struct = (double*) malloc(sizeof(double)*num_voxels);

	kernel_outputs *kern_out;
	kern_out = malloc(sizeof(kernel_outputs));
	kern_out->grad_theta = (double*) malloc(sizeof(double)*num_voxels);
	kern_out->grad_phi = (double*) malloc(sizeof(double)*num_voxels);
	kern_out->grad_a = (double*) malloc(sizeof(double)*num_voxels*p->num_opt_coeff);
	kern_out->synth_proj = (double*) malloc(sizeof(double)*p->dims_in->num_projections*(p->dims_in->sum_proj_size)*p->dims_in->num_segments);


	if (p->settings[3]){
		for (int i = 0; i < num_voxels; ++i)
		{
			kern_in->theta_struct[i] = opt_in->opt_vector[i];
		}

		for (int i = 0; i < num_voxels; ++i)
		{
			kern_in->phi_struct[i] = opt_in->opt_vector[num_voxels+i];
		}

		if ((p->settings[7]))
		{
			double tau = 2*pi;
			for (int i = 0; i < num_voxels; ++i)
			{
				kern_in->theta_struct[i] = fmod(kern_in->theta_struct[i],tau);
				kern_in->phi_struct[i] = fmod(kern_in->phi_struct[i],tau);
			}
		}

	} else {
		for (int i = 0; i < num_voxels; ++i)
			{
				kern_in->theta_struct[i] = s->theta[i];
				kern_in->phi_struct[i] = s->phi[i];
			}

	}
		
	kern_in->a = (double*) malloc(sizeof(double)*num_voxels*(p->num_opt_coeff));
		for (int j = 0; j < num_voxels; ++j)
			{
	for (int i = 0; i <(p->num_opt_coeff); ++i)
	{
		if (p->opt_coeff[i]){
		
				kern_in->a[i+j*p->num_opt_coeff] = opt_in->opt_vector[2*num_voxels + i+p->num_opt_coeff*j];
			}
		 else {	

				kern_in->a[i+j*p->num_opt_coeff] = s->a[i + p->num_opt_coeff*j];
			}
		}
	}


	//printf("Done with a!\n\n");
	double * sin_theta, *cos_theta, *sin_phi, *cos_phi;
	sin_theta = (double*) malloc(sizeof(double)*num_voxels);
	cos_theta = (double*) malloc(sizeof(double)*num_voxels);
	sin_phi = (double*) malloc(sizeof(double)*num_voxels);
	cos_phi = (double*) malloc(sizeof(double)*num_voxels);


	vdSin(num_voxels,kern_in->theta_struct,sin_theta);
	// vdAbs(num_voxels,sin_theta,sin_theta);
	vdCos(num_voxels,kern_in->theta_struct,cos_theta);
	// vdAbs(num_voxels,cos_theta,cos_theta);
	vdSin(num_voxels,kern_in->phi_struct,sin_phi);
	vdCos(num_voxels,kern_in->phi_struct,cos_phi);

	kern_in->rot_str = (double*) malloc(sizeof(double)*num_voxels*3*3);
	kern_in->rot_str_diff_theta = (double*) malloc(sizeof(double)*num_voxels*3*3);
	kern_in->rot_str_diff_phi = (double*) malloc(sizeof(double)*num_voxels*3*3);



	//Rotation matrices to get from spherical harmonic coordinates to object coordinates.
	//+0 included for legitbility and clarity.
	for (int i = 0; i < num_voxels; ++i)
	{
		kern_in->rot_str[i*9 + 0] = cos_theta[i]*cos_phi[i];
		kern_in->rot_str[i*9 + 1] = cos_theta[i]*sin_phi[i];
		kern_in->rot_str[i*9 + 2] = -sin_theta[i];
		kern_in->rot_str[i*9 + 3] = -sin_phi[i];
		kern_in->rot_str[i*9 + 4] = cos_phi[i];
		kern_in->rot_str[i*9 + 5] = 0.0;
		kern_in->rot_str[i*9 + 6] = sin_theta[i]*cos_phi[i];
		kern_in->rot_str[i*9 + 7] = sin_theta[i]*sin_phi[i];
		kern_in->rot_str[i*9 + 8] = cos_theta[i];

		kern_in->rot_str_diff_theta[i*9 + 0] = -sin_theta[i]*cos_phi[i];
		kern_in->rot_str_diff_theta[i*9 + 1] = -sin_theta[i]*sin_phi[i];
		kern_in->rot_str_diff_theta[i*9 + 2] = -cos_theta[i];
		kern_in->rot_str_diff_theta[i*9 + 3] = 0.0;
		kern_in->rot_str_diff_theta[i*9 + 4] = 0.0;
		kern_in->rot_str_diff_theta[i*9 + 5] = 0.0;
		kern_in->rot_str_diff_theta[i*9 + 6] = cos_theta[i]*cos_phi[i];
		kern_in->rot_str_diff_theta[i*9 + 7] = cos_theta[i]*sin_phi[i];
		kern_in->rot_str_diff_theta[i*9 + 8] = -sin_theta[i];

		kern_in->rot_str_diff_phi[i*9 + 0] = -cos_theta[i]*sin_phi[i];
		kern_in->rot_str_diff_phi[i*9 + 1] = cos_theta[i]*cos_phi[i];
		kern_in->rot_str_diff_phi[i*9 + 2] = 0.0;
		kern_in->rot_str_diff_phi[i*9 + 3] = -cos_phi[i];
		kern_in->rot_str_diff_phi[i*9 + 4] = -sin_phi[i];
		kern_in->rot_str_diff_phi[i*9 + 5] = 0.0;
		kern_in->rot_str_diff_phi[i*9 + 6] = -sin_theta[i]*sin_phi[i];
		kern_in->rot_str_diff_phi[i*9 + 7] = sin_theta[i]*cos_phi[i];
		kern_in->rot_str_diff_phi[i*9 + 8] = 0.0;
	}


	kern_in->unit_q_beamline = (double*) malloc(sizeof(double)*3*8);

	for (int i = 0; i < 8; ++i)
	{
		kern_in->unit_q_beamline[i*3 + 0] = sin(p->theta_det[0])*cos(p->phi_det[i]);
		kern_in->unit_q_beamline[i*3 + 1] = sin(p->theta_det[0])*sin(p->phi_det[i]);
		kern_in->unit_q_beamline[i*3 + 2] = cos(p->theta_det[0]);
	}
	_3dsaxs_error_metric_kernel(opt_in,kern_in,p,s,kern_out);
	printf("END\n");
	op->E = kern_out->E_out;
	op->E_reg = kern_out->E_reg;

	for (int i = 0; i < p->dims_in->num_projections; ++i)
	{
		for (int j = 0; j < p->dims_in->num_segments; ++j)
		{
			op->proj_out[i*p->dims_in->num_segments + j] = kern_out->synth_proj[i*p->dims_in->num_segments + j];
		}
	}
	if (p->settings[1])
	{
	for (int i = 0; i < num_voxels; ++i)
	{
		op->grad[i] = kern_out->grad_theta[i];
	}

	for (int i = 0; i < num_voxels; ++i)
	{
		op->grad[num_voxels + i] = kern_out->grad_phi[i];

	}

	for (int j = 0; j < num_voxels; ++j)
	{
		for (int i = 0; i < p->num_opt_coeff; ++i)
		{	if(p->opt_coeff[i]){
			op->grad[num_voxels*2 +j*p->num_opt_coeff + i] = kern_out->grad_a[j*p->num_opt_coeff + i];
			} else {
			op->grad[num_voxels*2 +j*p->num_opt_coeff + i] = 0.;	
			}
		}
	}
	}
	
	free(sin_theta);
	free(sin_phi);
	free(cos_theta);
	free(cos_phi);
	free(kern_in->theta_struct);
	free(kern_in->phi_struct);

	free(kern_out->grad_theta);
	free(kern_out->grad_phi);
	free(kern_out->grad_a);
	free(kern_out->synth_proj);
	free(kern_out);

	free(kern_in->unit_q_beamline);
	free(kern_in->rot_str);
	free(kern_in->rot_str_diff_phi);
	free(kern_in->rot_str_diff_theta);
	free(kern_in->a);
	free(kern_in);

}
