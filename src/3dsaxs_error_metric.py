# If this code, or subfunctions or parts of it, is used for research in a 
#   publication or if it is fully or partially rewritten for another 
#   computing language the authors and institution should be acknowledged 
#   in written form and additionally you should cite:
#     M. Liebi, M. Georgiadis, A. Menzel, P. Schneider, J. Kohlbrecher, 
#     O. Bunk, and M. Guizar-Sicairos, “Nanostructure surveys of 
#     macroscopic specimens by small-angle scattering tensor tomography,”
#     Nature 527, 349-352 (2015).   (doi:10.1038/nature16056)
#
#
#
#

import numpy as np
from numpy import pi,sin,cos
import param_struct as ps
from scipy.interpolate import RegularGridInterpolator
from scipy.signal import convolve2d,decimate
from scipy.spatial.transform import Rotation as Ro
from scipy.special import sph_harm
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from ctypes import *
import time
import ctypes
import arb_projection as ap

def error_metric(optim_in, p, s, projection, skip_optimization=False, find_grad=True, return_synth_proj=True, return_Ereg=True):
	coefficient_number = np.size(s.l)
	
	if skip_optimization:
		print("Skipping optimization!")
		find_coefficients = False
		find_orientation = False
		grad = []
		find_grad = False
		p.opt_coeff = False
	else:
		find_coefficients = np.any(p.opt_coeff)
		find_orientation = p.find_orientation
		if (p.regularization_angle) and not(find_orientation):
			print("p.regularization_angle was true but optimization of orientation (p.find_orientation) is false. Regularization of angle will be turned off as it does not make sense if the orienation is not optimized')")
			p.regularization_angle = 0

	phi_det = p.phi_det
	theta_det = np.ones(np.shape(phi_det)).*projection.
	nx=p.nx
	ny=p.ny
	nz=p.nz
	num_of_segments=p.num_of_segments
	num_of_voxels=p.num_of_voxels
	mask3D=s.mask3D
	num_of_pixels=np.size(projection)*nx*ny
	num_of_orders_opt=np.sum(p.opt_coeff>0)

	if find_orientation:
		theta_struct = optim_in[range(num_of_voxels)].reshape(nx,ny,nz)
		phi_struct = optim_in[slice(num_of_voxels,2*num_of_voxels)].reshape(nx,ny,nz)
		optim_in=optim_in[num_of_voxels*2:]
		if p.avoid_wrapping:
			phi_struct	%=	2*pi
			theta_struct %= 2*pi
	else:
		theta_struct = s.theta.data
		phi_struct = s.phi.data

	a=np.zeros(num_of_coeffs*num_of_voxels)
	for ii in range(np.size(p.opt_coeff)):
		if p.opt_coeff[ii]:
			a[ii:num_of_coeffs:ii+num_of_voxels] = optim_in[:num_of_voxels]
			optim_in=optim_in[num_of_voxels:]
		else:
			a[ii:num_of_coeffs:ii+num_of_voxels] = s.a[ii].data.flatten()

	grad_theta_struct = np.zeros((ny,nx,nz))
	grad_phi_struct = np.zeros((ny,nx,nz))

	sin_theta_struct = sin(theta_struct).flatten()
	cos_theta_struct = cos(theta_struct).flatten()
	sin_phi_struct = sin(phi_struct).flatten()
	cos_phi_struct = cos(phi_struct).flatten()

	zeros_struct = np.zeros(num_of_voxels)

	cos_th_cos_ph = cos_theta_struct*cos_phi_struct
	sin_th_cos_ph = sin_theta_struct*cos_phi_struct
	sin_th_sin_ph = sin_theta_struct*sin_phi_struct
	cos_th_sin_ph = cos_theta_struct*sin_phi_struct

	rot_str = np.dstack(
		cos_th_cos_ph, cos_th_sin_ph, -sin_theta_struct,
		-sin_phi_struct, cos_phi_struct, zeros_struct,
		sin_th_cos_ph, sin_th_sin_ph, cos_theta_struct
		).flatten()
	
	rot_str_diff_theta = np.dstack(
		-sin_th_cos_ph, -sin_th_sin_ph, -cos_theta_struct,
		zeros_struct, zeros_struct, zeros_struct,
		cos_th_cos_ph, cos_th_sin_ph, -sin_theta_struct
		).flatten()
	
	rot_str_diff_phi = np.dstack(
		-cos_th_sin_ph, cos_th_cos_ph, zeros_struct,
		-cos_phi_struct, -sin_phi_struct, zeros_struct,
		-sin_th_sin_ph, sin_th_cos_ph, zeros_struct
		).flatten()
	rot_str_dims = np.size(rot_str)
	data_size=np.size(projection[0].data)
	data_shape=np.shape(projection[0].data)
	data = np.zeros(np.size(projection)*np.size(projection[0].data))
	dy = np.zeros(np.size(projection)*np.size(projection[0].dy))
	dx = np.zeros(np.size(projection)*np.size(projection[0].dx))
	rot_exp = np.zeros(np.size(projection)*np.size(projection[0].rot_exp))
	window_mask = np.zeros(np.size(projection)*np.size(projection[0].window_mask))
	window_mask_size = np.size(projection[0].window_mask)
	for ii in range(np.size(projection))
		data[ii*data_size:(ii+1*data_size)] = projection[ii].data
		dy[ii:(ii+1)] = projection[ii].dy
		dx[ii:(ii+1)] = projection[ii].dx
		rot_exp[ii*9:(ii+1)*9] = projection[ii].rot_exp
		window_mask[ii*window_mask_size:(ii+1)*window_mask_size] = projection[ii].window_mask
	unit_q_beamline = np.concatenate(sin(theta_det)*cos(phi_det),sin(theta_det)*sin(phi_det),cos(theta_det))