#ifndef _arb_projection_h
#define _arb_projection_h

void arb_projection(double * restrict data_synt, double * restrict X, double * restrict Y,
 					double * restrict Z, double * R, double * xout, double * yout, double * projection_out, 
					int num_voxels, int num_segments, int * n_dims, int * n_xout_yout);

void arb_back_projection(double * restrict tomo_obj, double * restrict proj_obj, int * restrict proj_size, double * restrict X, double * restrict Y,
 					double * restrict Z, double * R, double * xout, double * yout,
					int num_voxels, int * n_dims, int num_segments);

#endif