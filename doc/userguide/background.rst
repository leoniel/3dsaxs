.. _background:
.. index:: Background

Background
**********

xxx

.. math::

    E = E(\{r_{ij}\})
    = E_0
    + \sum_{ij} \phi_{ij} f(r_{ij})
    + \sum_{ij,kl} \phi_{ij,kl} f(r_{ij}) f(r_{kl})
    + \sum_{ij,kl,mn} \phi_{ij,kl,mn} f(r_{ij}) f(r_{kl}) f(r_{mn}) \ldots

For example, a term :math:`\phi_{ij} f(r_{ij})` ...