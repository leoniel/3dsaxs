.. index:: Glossary

Glossary
********


General
=======
.. glossary::

   SAXS
        `small angle X-ray scattering <https://en.wikipedia.org/wiki/Small-angle_X-ray_scattering>`_