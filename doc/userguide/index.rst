:program:`pysaxs` — XXX
***********************

:program:`pysaxs` is a tool ...

.. toctree::
   :maxdepth: 2
   :caption: Main

   background

.. toctree::
   :maxdepth: 2
   :caption: Function reference

   moduleref/index

.. toctree::
   :maxdepth: 2
   :caption: Backmatter

   bibliography
   glossary
   genindex
