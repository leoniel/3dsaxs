.. index::
   single: GraphCalculator

GraphCalculator
===============

.. module:: grappy
   :noindex:

.. autoclass:: GraphCalculator
   :members:
   :undoc-members:
   :inherited-members:
