.. index::
   single: GraphModel

GraphModel
==========

.. module:: grappy
   :noindex:

.. autoclass:: GraphModel
   :members:
   :undoc-members:
   :inherited-members:
