.. index::
   single: GraphExpansion

GraphExpansion
==============

.. module:: grappy
   :noindex:

.. autoclass:: GraphExpansion
   :members:
   :undoc-members:
   :inherited-members:
