.. index::
   single: DummyClass

Description of DummyClass
=========================

.. module:: pysaxs
   :noindex:

.. autoclass:: pysaxs.DummyClass
   :members:
   :undoc-members:
   :inherited-members:
