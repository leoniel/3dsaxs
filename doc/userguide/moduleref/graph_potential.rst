.. index::
   single: GraphPotential

GraphPotential
==============

.. module:: grappy
   :noindex:

.. autoclass:: GraphPotential
   :members:
   :undoc-members:
   :inherited-members:
