from .dummy import DummyClass

__project__ = 'pysaxs'
__description__ = 'XXX'
__copyright__ = '2020'
__license__ = 'MIT'
__version__ = '0.1'
__all__ = ['DummyClass']  # populate me
__status__ = 'alpha-version'
__maintainer__ = 'PYSAXS developers group'
__url__ = 'http://pysaxs.materialsmodeling.org/'
