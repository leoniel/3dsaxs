"""
I am the home of DummyClass.
"""

class DummyClass:

    """Test docstring.

    Attributes
    ----------
    dog_name
        name of the dog

    Examples
    --------

    This is an example::

        >>> c = DummyClass('xyz')
        >>> print(c.dog_name)
    """

    def __init__(self, name: str, age: int) -> None:
        self.dog_name = name
        self._name = name
        self._age = age

    @property
    def age(self) -> int:
        """ Age """
        return self._age

    @property
    def name(self) -> str:
        return self._name

    def get_older(self) -> None:
        self._age += 1
